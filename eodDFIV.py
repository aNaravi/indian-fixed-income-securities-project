import os, urllib, re, psycopg2
from selenium import webdriver
from archive import format2

driver = webdriver.PhantomJS( executable_path='/home/akhil/Documents/Programming/WebDrivers/phantomjs',\
                        service_args=['--ignore-ssl-errors=true'])

driver.get('https://www.ccilindia.com/RESEARCH/STATISTICS/Pages/Infovendors.aspx')
url = driver.find_element_by_link_text('Daily Data For InfoVendors').get_attribute('href')

dirName = '/tmp/EOD/'
p = re.compile(r'DFIV.+')
filename = p.search( url ).group()

os.mkdir(dirName)
os.chdir(dirName)
urllib.urlretrieve(url, filename)

files = [ f for f in os.listdir(dirName) if os.path.isfile(os.path.join(dirName, f)) ]

months = { re.compile(r'DFIV_\d{,2}_01_\d{,4}.zip') : '01-', re.compile(r'DFIV_\d{,2}_02_\d{,4}.zip') : '02-', \
        re.compile(r'DFIV_\d{,2}_03_\d{,4}.zip') : '03-', re.compile(r'DFIV_\d{,2}_04_\d{,4}.zip') : '04-', \
        re.compile(r'DFIV_\d{,2}_05_\d{,4}.zip') : '05-', re.compile(r'DFIV_\d{,2}_06_\d{,4}.zip') : '06-', \
        re.compile(r'DFIV_\d{,2}_07_\d{,4}.zip') : '07-', re.compile(r'DFIV_\d{,2}_08_\d{,4}.zip') : '08-', \
        re.compile(r'DFIV_\d{,2}_09_\d{,4}.zip') : '09-', re.compile(r'DFIV_\d{,2}_10_\d{,4}.zip') : '10-', \
        re.compile(r'DFIV_\d{,2}_11_\d{,4}.zip') : '11-', re.compile(r'DFIV_\d{,2}_12_\d{,4}.zip') : '12-' }

years = { re.compile(r'.+2011.zip') : '2011-', re.compile(r'.+2012.zip') : '2012-', re.compile(r'.+2013.zip') : '2013-', \
        re.compile(r'.+2014.zip') :'2014-' }

for f in files:
    for m in months.iterkeys():
        month_match = m.match(f)
        if month_match != None:
            for y in years.iterkeys():
                year_match = y.match(f)
                if year_match != None:
                    newFileName = "DFIV_"+years[y]+months[m]+f[5:7]+".zip"
                    os.rename(f, newFileName)
                    break
            break


con = psycopg2.connect(database = 'archive', user = 'akhil')
cur = con.cursor()
f = format2(dirName, cur, con)
f.read()
f.write_to_db()

for f in os.listdir(dirName):
    os.remove(f)
