# Define your item pipelines here
#
# Don't forget to add your pipeline to the item_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import datetime as dt
import psycopg2
import re

def get_timestamp(ts, y, m, d):

    ts = ts.split(':')
    hours, minutes, seconds = int(ts[0].split(' ')[-1]), int(ts[1]), int(ts[2].split(' ')[0])

    hours =  hours * ( hours != 12 ) + ( ( ts[2].split(' ')[1] == 'PM' ) * 12 ) #12hr to 24hr format

    return dt.datetime( y, m, d, hours, minutes, seconds )


def get_maturity(date):

    months_map = { 'Jan':1, 'Feb':2, 'Mar':3, 'Apr':4, 'May':5, 'Jun':6, 'Jul':7, 'Aug':8, 'Sep':9, 'Oct':10, 'Nov':11, 'Dec':12 }
    return dt.datetime( int(date[2]), months_map[date[1]], int(date[0]) ).date()


class ccilPipeline(object):

    def db_connection(self, username, database):

        self.con = psycopg2.connect(database = database, user = username)
        self.cur = self.con.cursor()


    def close_spider(self, spider):

        self.cur.close()
        self.con.close()


    def process_item(self, item, spider):
        if ( spider.name == 'om_mkt_by_price' ):
            self.process_om_mkt_by_price( item )

        elif ( spider.name == 'om_trades' ):
            self.process_om_trades( item )

        elif ( spider.name == 'call_deals' ):
            self.process_call_deals( item )

        elif ( spider.name == 'call_mkt_by_rate' ):
            self.process_call_mkt_by_rate( item )

        elif ( spider.name == 'fimmda' ):
            self.process_fimmda( item )

        elif ( spider.name == 'derivatives' ):
            self.process_derivatives( item )

        elif ( spider.name == 'forex' ):
            self.process_forex( item )

        elif ( spider.name == 'cblo' ):
            self.process_cblo( item )

        elif ( spider.name == 'croms_mkt_by_rate' ):
            self.process_croms_mkt_by_rate( item )

        elif ( spider.name == 'croms_deals' ):
            self.process_croms_deals( item )

        elif ( spider.name == 'om_reported_deals' ):
            self.process_om_reported_deals( item )

        elif ( spider.name == 'call_reported_deals' ):
            self.process_call_reported_deals( item )

        elif ( spider.name == 'croms_reported_deals' ):
            self.process_croms_reported_deals( item )


    def process_om_mkt_by_price(self, mkt):

        self.db_connection(mkt['username'][0], mkt['database'][0])

        column_str, insert_str, query = dict(), dict(), dict()

        column_str['om_summary'] = 'sr_no, ts, market_type, sec_type, sub_type, security_name,\
									open, yield_open,\
									high, yield_high,\
									low, yield_low,\
									close, yield_close,\
									total_traded_amount, trades'
        insert_str['om_summary'] = ('%s, '*16)[:-2]
        query['om_summary'] = 'INSERT INTO om_summary (%s) VALUES (%s)' % ( column_str['om_summary'], insert_str['om_summary'] )

        column_str['om_quotes'] = 'sr_no, ts, market_type, sec_type, sub_type, security_name,\
									bid_nos, bid_amt, bid_yield, bid_price,\
									offer_price, offer_yield, offer_amt, offer_nos'
        insert_str['om_quotes'] = ('%s, '*14)[:-2]
        query['om_quotes'] = 'INSERT INTO om_quotes (%s) VALUES (%s)' % ( column_str['om_quotes'], insert_str['om_quotes'] )

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        if ( mkt.has_key('quotes') ):
            for q in mkt['quotes']:

                q[0], q[1] = int(q[0]), get_timestamp(q[1], y, m, d)

                try:
                    q[6], q[7], q[8], q[9] = \
						int(q[6]), float(q[7]), float(q[8]), float(q[9])
                except:
                    q[6], q[7], q[8], q[9] = None, None, None, None

                try:
                    q[10], q[11], q[12], q[13] = \
						float(q[10]), float(q[11]), float(q[12]), int(q[13])
                except:
                    q[10], q[11], q[12], q[13] = None, None, None, None

                self.cur.execute( query['om_quotes'], tuple(q) )

            self.con.commit()

        if ( mkt.has_key('summary') ):
            for s in mkt['summary']:

                s[0], s[1] = int(s[0]), get_timestamp(s[1], y, m, d)

                s[6], s[7], s[8], s[9] = s[6].split('/'), s[7].split('/'), s[8].split('/'), s[9].split('/')

                s[6], s[7], s[8], s[9] = [float(s[6][0]), float(s[6][1])], [float(s[7][0]), float(s[7][1])], \
										 [float(s[8][0]), float(s[8][1])], [float(s[9][0]), float(s[9][1])]

                s[10], s[11] = float(s[10]), int(s[11])

                self.cur.execute( query['om_summary'], tuple( sum( [s[:6], s[6], s[7], s[8], s[9], s[10:]], [] ) ) )
            self.con.commit()


    def process_om_trades(self, trades):

        self.db_connection(trades['username'][0], trades['database'][0])

        column_str = 'market_type, sec_type, sub_type, security_name, ts, amount, price, yield'
        insert_str = ('%s, '*8)[:-2]
        query = 'INSERT INTO %s (%s) VALUES (%s)' % (trades['table'][0], column_str, insert_str)

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        if ( trades.has_key('trades') ):
            for t in trades['trades']:
                t[4] = dt.datetime( y, m, d, *[ int(x) for x in t[4].split(':') ] )
                self.cur.execute( query, tuple( [t[0], t[1], t[2], t[3], t[4], float(t[5]), float(t[6]), float(t[7])] ) )

        self.con.commit()


    def process_om_reported_deals(self, deals):

        self.db_connection(deals['username'][0], deals['database'][0])

        column_str = 'security_name, maturity_date, ts, amount, price, yield'
        insert_str = ('%s, '*6)[:-2]
        query = 'INSERT INTO %s (%s) VALUES (%s)' % (deals['table'][0], column_str, insert_str)

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        if ( deals.has_key('deals') ):
            for i in deals['deals']:
                i[2] = dt.datetime( y, m, d, *[ int(x) for x in i[2].split(':') ] )
                i[3:] = [ float(x) for x in i[3:] ]
                self.cur.execute( query, tuple(i) )

            self.con.commit()


    def process_call_mkt_by_rate(self, mkt):

        self.db_connection(mkt['username'][0], mkt['database'][0])

        column_str, insert_str, query = dict(), dict(), dict()
        column_str['call_summary'] = 'sr_no, ts, sett_type, contract_type, tenor,\
						            maturity_date, open, high, low, \
						            last_trade_rate, last_trade_amount, last_trade_timestamp,\
                                    total_traded_amount, weighted_avg_rate'
        insert_str['call_summary'] = ('%s, '*14)[:-2]
        query['call_summary'] = 'INSERT INTO call_summary (%s) VALUES (%s)' %\
                                    (column_str['call_summary'], insert_str['call_summary'])

        column_str['call_quotes'] = 'sr_no, ts, sett_type, contract_type, tenor,\
						            maturity_date, borrow_amt, borrow_rate, lend_rate, lend_amt'
        insert_str['call_quotes'] = ('%s, '*10)[:-2]
        query['call_quotes'] = 'INSERT INTO call_quotes (%s) VALUES (%s)' % (column_str['call_quotes'], insert_str['call_quotes'])

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        if ( mkt.has_key('summary') ):
            for s in mkt['summary']:

                s[0], s[1] = int(s[0]), get_timestamp(s[1], y, m, d)
                s[5] = dt.datetime( *[ int(x) for x in s[5].split('/')[::-1] ] ).date()

                s[6], s[7], s[8], s[9], s[10], s[12], s[13] = \
                    float(s[6]), float(s[7]), float(s[8]), float(s[9]), float(s[10]), float(s[12]), float(s[13])
                s[11] = dt.time( *[ int(x) for x in s[11].split(':') ] )

                self.cur.execute( query['call_summary'], tuple(s) )

            self.con.commit()

        if ( mkt.has_key('quotes') ):
            for q in mkt['quotes']:

                q[0], q[1] = int(q[0]), get_timestamp(q[1], y, m, d)
                q[5] = dt.datetime( *[ int(x) for x in q[5].split('/')[::-1] ] ).date()

                try:
                    q[6:8] = [ float(x) for x in q[6:8] ]
                except (ValueError):
                    q[6:8] = [None]*2
                try:
                    q[8:10] = [ float(x) for x in q[8:10] ]
                except (ValueError):
                    q[8:10] = [None]*2

                self.cur.execute( query['call_quotes'], tuple(q) )

            self.con.commit()

        self.cur.close()
        self.con.close()


    def process_call_deals(self, deals):

        self.db_connection(deals['username'][0], deals['database'][0])

        column_str = 'sett_type, contract_type, tenor, maturity_date, ts, amount, rate'
        insert_str = ('%s, '*7)[:-2]
        query = 'INSERT INTO %s (%s) VALUES (%s)' % (deals['table'][0], column_str, insert_str)

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        if ( deals.has_key('deals') ):
            for i in deals['deals']:
                i[3] = dt.datetime( *[ int(x) for x in i[3].split('/')[::-1] ] ).date()
                i[4] = dt.datetime( y, m, d, *[ int(x) for x in i[4].split(':') ] )
                self.cur.execute( query, tuple( [i[0], i[1], i[2], i[3], i[4], float(i[5]), float(i[6])] ) )

        self.con.commit()


    def process_call_reported_deals(self, deals):

        self.db_connection(deals['username'][0], deals['database'][0])

        column_str = 'tenor, sett_type, maturity_date, ts, amount, rate'
        insert_str = ('%s, '*6)[:-2]
        query = 'INSERT INTO %s (%s) VALUES (%s)' % (deals['table'][0], column_str, insert_str)

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        if ( deals.has_key('deals') ):
            for i in deals['deals']:
                i[3] = dt.datetime( y, m, d, *[ int(x) for x in i[3].split(':') ] )
                i[4:] = [ float(x) for x in i[4:] ]
                self.cur.execute( query, tuple(i) )

            self.con.commit()


    def process_croms_mkt_by_rate(self, mkt):

        self.db_connection(mkt['username'][0], mkt['database'][0])

        column_str, insert_str, query = dict(), dict(), dict()

        column_str['summary'] = 'sr_no, ts, repo_type, security, sett_type, tenor, maturity,\
                                open, high, low, last_trade_rate, last_trade_amount, last_trade_timestamp,\
                                total_traded_amount, weighted_avg_rate'
        column_str['quotes'] = 'sr_no, ts, repo_type, security, sett_type, tenor, maturity,\
                                borrow_amt, borrow_price, borrow_rate, lend_rate, lend_price, lend_amt'

        insert_str['summary'] = ('%s, '*15)[:-2]
        insert_str['quotes'] = ('%s, '*13)[:-2]

        query['summary'] = 'INSERT INTO croms_summary (%s) VALUES (%s)' % ( column_str['summary'], insert_str['summary'] )
        query['quotes'] = 'INSERT INTO croms_quotes (%s) VALUES (%s)' % ( column_str['quotes'], insert_str['quotes'] )

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        if( mkt.has_key('summary') ):
            for row in mkt['summary']:
                row[1], row[3], row[5] = get_timestamp(row[1], y, m, d), row[3].split('  ')[0], row[5].split('  ')[0]
                row[6] = dt.datetime( *[ int(x) for x in row[6].split('/')[::-1] ] ).date()

                row[7:12], row[12], row[13:] = [ float(x) for x in row[7:12] ], \
                                                dt.datetime( y, m, d, *[ int(x) for x in row[12].split(':') ] ), \
                                                [ float(x) for x in row[13:] ]

                self.cur.execute( query['summary'], tuple(row) )

            self.con.commit()

        if( mkt.has_key('quotes') ):
            for row in mkt['quotes']:
                row[1], row[3], row[5] = get_timestamp(row[1], y, m, d), row[3].split('  ')[0], row[5].split('  ')[0]
                row[6] = dt.datetime( *[ int(x) for x in row[6].split('/')[::-1] ] ).date()
                row[7:] = [ float(x) if x!='-' else None for x in row[7:] ]

                self.cur.execute( query['quotes'], tuple(row) )

            self.con.commit()


    def process_croms_deals(self, deals):

        self.db_connection(deals['username'][0], deals['database'][0])

        column_str = 'repo_type, security_name, sett_type, tenor, repo_maturity_date, ts, amount, rate, price'
        insert_str = ('%s, '*9)[:-2]
        query = 'INSERT INTO %s (%s) VALUES (%s)' % (deals['table'][0], column_str, insert_str)

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        if ( deals.has_key('deals') ):
            for row in deals['deals']:
                row[5] = dt.datetime( y, m, d, *[ int(x) for x in row[5].split(':') ] )
                row[6:] = [ float(x) if x != '-' else None for x in row[6:] ]

                self.cur.execute( query, tuple(row) )

            self.con.commit()


    def process_croms_reported_deals(self, deals):

        self.db_connection(deals['username'][0], deals['database'][0])

        column_str = 'security_name, maturity_date, tenor, sett_type, repo_maturity_date, ts, amount, rate, price'
        insert_str = ('%s, '*9)[:-2]
        query = 'INSERT INTO %s (%s) VALUES (%s)' % (deals['table'][0], column_str, insert_str)

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        if ( deals.has_key('deals') ):
            for i in deals['deals']:
                i[5] = dt.datetime( y, m, d, *[ int(x) for x in i[5].split(':') ] )
                i[6:] = [ float(x) for x in i[6:] ]
                self.cur.execute( query, tuple(i) )

            self.con.commit()


    def process_fimmda(self, fimmda):

        self.db_connection(fimmda['username'][0], fimmda['database'][0])

        column_str, insert_str, query = dict(), dict(), dict()
        column_str['fimmda_cd_cp'] = 'ts, isin, issuer, type, maturity_date, resi_days, sett_type, trades,\
						            total_traded_amount, last_trade_price, last_trade_yield, open, high, low,\
                                    weighted_avg_price, weighted_avg_yield'
        insert_str['fimmda_cd_cp'] = ('%s, '*16)[:-2]
        column_str['fimmda_cb'] = 'ts, isin, description, maturity_date, sett_type, tenor, repo_mat.date, trades,\
						            total_traded_amount, total_traded_amount_fv, last_trade_rate, last_trade_price,\
                                    open, high, low, weighted_avg_rate'
        insert_str['fimmda_cb'] = ('%s, '*16)[:-2]

        query['fimmda_cd_cp'] = 'INSERT INTO fimmda_cd_cp (%s) VALUES (%s)' % (column_str['fimmda_cd_cp'], insert_str['fimmda_cd_cp'])
        query['fimmda_cb'] = 'INSERT INTO fimmda_cb (%s) VALUES (%s)' % (column_str['fimmda_cb'], insert_str['fimmda_cb'])


        cd_re = re.compile(' CD .+')
        cd_strip_issuer = lambda s: s.replace( cd_re.search(s).group(), '' )
        cp_re = re.compile(' \d+D CP .+')
        cp_strip_issuer = lambda s: s.replace( cp_re.search(s).group(), '' )

        if ( fimmda.has_key('cd') ):
            for row in fimmda['cd']:
                row[2] = cd_strip_issuer(row[2])
                row[4] = get_maturity(row[4].split('-'))
                row[5], row[7], row[8] = int(row[5]), int(row[7]), float(row[8].replace(',', ''))
                row[9:] = [ float(x) for x in row[9:] ]
                self.cur.execute( query['fimmda_cd_cp'], tuple(row) )

            self.con.commit()

        if ( fimmda.has_key('cp') ):
            for row in fimmda['cp']:
                row[2] = cp_strip_issuer(row[2])
                row[4] = get_maturity(row[4].split('-'))
                row[5], row[7], row[8] = int(row[5]), int(row[7]), float(row[8].replace(',', ''))
                row[9:] = [ float(x) for x in row[9:] ]
                self.cur.execute( query['fimmda_cd_cp'], tuple(row) )

            self.con.commit()

        if ( fimmda.has_key('cb') ):
            for row in fimmda['cb']:
                print row
                '''row[3] = get_maturity(row[3].split('-'))
                row[4], row[5], row[7] = int(row[4]), int(row[5]), int(row[7])
                row[8:] = [ float(x) for x in row[8:] ]
                self.cur.execute( query['fimmda_cb'], tuple(row) )

            self.con.commit()'''


    def process_derivatives(self, derivatives):

        self.db_connection(derivatives['username'][0], derivatives['database'][0])

        column_str, query = dict(), dict()

        column_str['irs_ff'] = 'sr_no, benchmark, ts, maturity, last_reported_prev, high, low, wt_avg, last_reported_curr, volume, trades'
        column_str['fra'] = 'sr_no, benchmark, ts, tenor, last_reported_prev, high, low, wt_avg, last_reported_curr, volume, trades'
        insert_str = ('%s, '*11)[:-2]
        query['irs_ff'] = 'INSERT INTO irs_ff (%s) VALUES (%s)' % (column_str['irs_ff'], insert_str)
        query['fra'] = 'INSERT INTO fra (%s) VALUES (%s)' % (column_str['fra'], insert_str)

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        if( derivatives.has_key('irs_ff') ):
            for row in derivatives['irs_ff']:
                row[2] = get_timestamp(row[2], y, m, d)
                row[4:] = [ float(x)  if x!=' ' else None for x in row[4:]]
                self.cur.execute( query['irs_ff'], tuple(row) )

            self.con.commit()

        if( derivatives.has_key('fra') ):
            for row in derivatives['fra']:
                row[2] = get_timestamp(row[2], y, m, d)
                row[4:] = [ float(x)  if x!=' ' else None for x in row[6:] ]
                self.cur.execute( query['fra'], tuple(row[:-2]) )

            self.con.commit()


    def process_cblo(self, cblo):

        self.db_connection(cblo['username'][0], cblo['database'][0])

        column_str, insert_str, query = dict(), dict(), dict()

        column_str['summary'] = 'sr_no, ts, cblo_id, sett_type, open, high, low, weighted_avg_rate, last_trade_yield, last_trade_amount,\
                                last_trade_timestamp, trades, total_traded_amount'
        insert_str['summary'] = ('%s, '*13)[:-2]
        column_str['quotes'] = 'sr_no, cblo_id, ts, lending_no, lending_amt, lending_rate, borrowing_rate, borrowing_amt, borrowing_no'
        insert_str['quotes'] = ('%s, '*9)[:-2]

        query['summary'] = 'INSERT INTO cblo_summary (%s) VALUES (%s)' % (column_str['summary'], insert_str['summary'])
        query['quotes'] = 'INSERT INTO cblo_quotes (%s) VALUES (%s)' % (column_str['quotes'], insert_str['quotes'])

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        if( cblo.has_key('summary1') and cblo.has_key('summary2') ):
            for s1,s2 in zip(cblo['summary1'],cblo['summary2']):
                row = []
                row.append(s1[0])
                ts = s1[1].split(':')
                row.append( dt.datetime( y, m, d, int(ts[0].split(' ')[-1]), int(ts[1]), int(ts[2].split(' ')[0]) ) )
                row.extend( s1[2:4] )
                row.extend( [ float(x) if x != '-' else None for x in s1[4:10] ] )
                row.append( dt.datetime( y, m, d, *( [ int(x) for x in s1[10].split(':') ] ) ) )
                row.extend( [ float(x) for x in [ filter( lambda s: s!=',', y ) for y in s2[-2:] ] ] )

                self.cur.execute( query['summary'], tuple(row) )

            self.con.commit()

        if( cblo.has_key('quotes') ):
            for q in cblo['quotes']:
                row = []
                row.extend( q[:2] )
                ts = q[2].split(':')
                row.append( dt.datetime( y, m, d, int(ts[0].split(' ')[-1]), int(ts[1]), int(ts[2].split(' ')[0]) ) )
                row.extend( [ float(x.replace(',','')) if x != '-' else None for x in q[3:] ] )

                self.cur.execute( query['quotes'], tuple(row) )

            self.con.commit()


    def process_forex(self, forex):

        self.db_connection(forex['username'][0], forex['database'][0])

        column_str, insert_str, query = dict(), dict(), dict()

        column_str['summary'] = 'sr_no, ts, open, high, low, close'
        insert_str['summary'] = ('%s, '*6)[:-2]
        column_str['quotes'] = 'sr_no, ts, bid_orders, bid_quantity, bid_price, offer_price, offer_quantity, offer_orders'
        insert_str['quotes'] = ('%s, '*8)[:-2]

        query['summary'] = 'INSERT INTO forex_spot_summary (%s) VALUES (%s)' % (column_str['summary'], insert_str['summary'])
        query['quotes'] = 'INSERT INTO forex_spot_quotes (%s) VALUES (%s)' % (column_str['quotes'], insert_str['quotes'])

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        if( forex.has_key('summary') ):
            for s in forex['summary']:
                s[1] = get_timestamp(s[1], y, m, d)
                s[2:] = [ float(x) if x != '-' else None for x in s[2:] ]

                self.cur.execute( query['summary'], tuple(s) )

            self.con.commit()

        if( forex.has_key('quotes') ):
            for q in forex['quotes']:
                q[1] = get_timestamp(q[1], y, m, d)
                q[2:] = [ float(x) if x != '-' else None for x in q[2:] ]

                self.cur.execute( query['quotes'], tuple(q) )

            self.con.commit()


