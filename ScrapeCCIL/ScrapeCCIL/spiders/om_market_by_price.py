from scrapy.spider import Spider
from scrapy import selector
from scrapy.item import Item, Field
from scrapy.contrib.loader import ItemLoader, processor
from ScrapeCCIL.items import OM_MKT_BY_PRICE

from selenium import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium import common
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import *
from selenium.webdriver.support.select import Select

from exceptions import IndexError, TypeError
import psycopg2
import datetime as dt
import time

class mkt_by_price_Spider(Spider):

    name = 'om_mkt_by_price'
    start_urls = ['https://www.ccilindia.com/OMMBP.aspx']

    def __init__(self, username, database):

        self.username, self.database = username, database

        self.con = psycopg2.connect(database = database, user = username)
        self.cur = self.con.cursor()
        self.cur.execute('''CREATE TABLE IF NOT EXISTS om_summary
		(
			sr_no integer,
			ts timestamp without time zone NOT NULL,
            market_type character varying NOT NULL,
			sec_type character varying NOT NULL,
			sub_type character varying NOT NULL,
			security_name character varying NOT NULL,
			open real NOT NULL,
			high real NOT NULL,
			low real NOT NULL,
			close real NOT NULL,
			yield_open real NOT NULL,
			yield_high real NOT NULL,
			yield_low real NOT NULL,
			yield_close real NOT NULL,
			total_traded_amount real NOT NULL,
			trades integer NOT NULL
		)''')

        self.con.commit()

        self.cur.execute('''CREATE TABLE IF NOT EXISTS om_quotes
		(
			sr_no integer,
			ts timestamp without time zone NOT NULL,
            market_type character varying NOT NULL,
			sec_type character varying NOT NULL,
			sub_type character varying NOT NULL,
			security_name character varying NOT NULL,
			bid_nos integer,
			bid_amt real,
			bid_yield real,
			bid_price real,
			offer_price real,
			offer_yield real,
			offer_amt real,
			offer_nos integer
		)''')

        self.con.commit()

        Spider.__init__(self)

        self.selenium = webdriver.PhantomJS( executable_path='/home/akhil/Documents/Programming/WebDrivers/phantomjs',\
                                             service_args=['--ignore-ssl-errors=true'])


    def __del__(self):
        self.cur.close()
        self.con.close()
        self.selenium.quit()


    def parse(self, response):

        self.cur.execute('''select max(sr_no) from om_quotes where ts >= %s''', [dt.datetime.date(dt.datetime.today())] )
        try:
            sr_no = self.cur.fetchone()[0] + 1
        except (TypeError):
            sr_no = 1

        mkt = OM_MKT_BY_PRICE()
        l = ItemLoader( item = mkt, response=response )

        l.add_value('username', self.username)
        l.add_value('database', self.database)

        self.selenium.get(response.url)

        xpathMarketType = '//*[@id="ddlMRKT_INDC"]'
        xpathSecType = '//*[@id="ddlISMT_INDC"]'
        xpathSubType = '//*[@id="ddlBOOK_INDC"]'
        xpathSecurities = '//*[@id="ddlISMT_IDNT"]'
        xpathTextField = '//*[@id="lblISMT_IDNT"]'
        xpathGO = '//*[@id="btnDisplay"]'

        eMarketType = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlMRKT_INDC"]')
        eSecTypes = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlISMT_INDC"]')
        eSubTypes = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlBOOK_INDC"]')
        eSecurities = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlISMT_IDNT"]')

        eGO = lambda: self.selenium.find_element_by_xpath('//*[@id="btnDisplay"]')

        eTimestamp = lambda: self.selenium.find_element_by_xpath('//*[@id="lblTime"]')
        eSummaryTable= lambda: self.selenium.find_elements_by_xpath('//table[@id="grdMBPTop"]//tr')
        eBidTable=  lambda: self.selenium.find_elements_by_xpath('//table[@id="grdMBPBid" or @id="grdMBPBidTB"]//tr')[1:-2]
        eOfferTable=  lambda: self.selenium.find_elements_by_xpath('//table[@id="grdMBPOffer" or @id="grdMBPOfferTB"]/tbody//tr')[1:-2]


        marketTypes = [ x.text for x in Select( eMarketType() ).options ]

        for i1 in marketTypes:

            Select( eMarketType() ).select_by_visible_text( i1 )

            WebDriverWait( self.selenium, 5 ).until( lambda x: x.find_element_by_xpath( xpathTextField ).text=='' )

            secTypes = [ x.text for x in Select( eSecTypes() ).options[1:] ]

            for i2 in secTypes:
                flag = 0

                Select( eSecTypes() ).select_by_visible_text( i2 )

                WebDriverWait(self.selenium, 5).until( lambda x: x.find_element_by_xpath( xpathTextField ).text=='' )

                if( i2 != secTypes[0] ):
                    try:
                        eGO().click()
                        if( not(eGO().is_enabled()) ): flag = 1
                    except :
                        flag = 1 #NO SECURITY IN PREV ODD LOT

                subTypes = [ x.text for x in Select( eSubTypes() ).options ]

                if( i1 == marketTypes[1] ): subTypes.pop()

                for i3 in subTypes:

                    Select( eSubTypes() ).select_by_visible_text( i3 )

                    WebDriverWait(self.selenium, 5).until( lambda x: x.find_element_by_xpath( xpathTextField ).text=='' )

                    if ( flag == 1 ):
                        try:
                            WebDriverWait(self.selenium, 5).until_not( lambda x: x.find_element_by_xpath( xpathSecurities ).text=='' )
                            flag = 0
                        except (TimeoutException):
                            continue
                        except (StaleElementReferenceException):
                            pass

                    securities = [ x.text for x in Select( eSecurities() ).options ]

                    for i4 in securities:

                        Select( eSecurities() ).select_by_visible_text( i4 )

                        eGO().click()

                        try:
                            WebDriverWait(self.selenium, 7).until( EC.text_to_be_present_in_element( (By.XPATH, xpathTextField ), i4 ) )
                        except:
                            print 'TimeoutException: ', i1, i2, i3, i4
                            continue

                        l.add_value( 'summary', [sum( [ [sr_no, eTimestamp().text, i1, i2, i3, i4], eSummaryTable()[1].text.split(' ')], [] )] )

                        for b,o in zip( eBidTable(), eOfferTable() ):
                            l.add_value( 'quotes', [sum( [ [sr_no, eTimestamp().text, i1, i2, i3, i4], b.text.split(' '), o.text.split(' ') ], [] )] )

        l.load_item()
        self.__del__()
        return mkt
