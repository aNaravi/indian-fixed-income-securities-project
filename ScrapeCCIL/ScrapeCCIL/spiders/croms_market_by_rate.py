from scrapy.spider import Spider
from scrapy import selector
from scrapy.item import Item, Field
from scrapy.contrib.loader import ItemLoader, processor
from ScrapeCCIL.items import CROMS_MKT_BY_RATE

from selenium import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium import common
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from selenium.webdriver.support.select import Select

from exceptions import IndexError, TypeError
import psycopg2
import datetime as dt
import time

class croms_mkt_by_rate(Spider):

    name = 'croms_mkt_by_rate'
    start_urls = ['https://www.ccilindia.com/RepoMBR.aspx']

    def __init__(self, username, database):

        self.username, self.database = username, database

        self.con = psycopg2.connect(database = database, user = username)
        self.cur = self.con.cursor()

        self.cur.execute('''CREATE TABLE IF NOT EXISTS croms_summary
		(
			sr_no integer,
			ts timestamp without time zone NOT NULL,
            repo_type character varying NOT NULL,
			security character varying NOT NULL,
			sett_type character varying NOT NULL,
			tenor character varying NOT NULL,
            maturity date NOT NULL,
			open real NOT NULL,
			high real NOT NULL,
			low real NOT NULL,
			last_trade_rate real NOT NULL,
			last_trade_amount real NOT NULL,
			last_trade_timestamp timestamp without time zone  NOT NULL,
			total_traded_amount real NOT NULL,
			weighted_avg_rate real NOT NULL
		)''')

        self.con.commit()

        self.cur.execute('''CREATE TABLE IF NOT EXISTS croms_quotes
		(
			sr_no integer,
			ts timestamp without time zone NOT NULL,
            repo_type character varying NOT NULL,
			security character varying NOT NULL,
			sett_type character varying NOT NULL,
			tenor character varying NOT NULL,
            maturity date NOT NULL,
			borrow_amt real,
			borrow_price real,
			borrow_rate real,
			lend_rate real,
			lend_price real,
			lend_amt real
		)''')

        self.con.commit()

        Spider.__init__(self)
        self.selenium = webdriver.PhantomJS( executable_path='/home/akhil/Documents/Programming/WebDrivers/phantomjs',\
                                             service_args=['--ignore-ssl-errors=true'])


    def __del__(self):
        self.cur.close()
        self.con.close()
        self.selenium.quit()


    def parse(self, response):

        self.cur.execute('''select max(sr_no) from croms_quotes where ts >= %s''', [ dt.datetime.today().date() ] )
        try:
            sr_no = self.cur.fetchone()[0] + 1
        except (TypeError):
            sr_no = 1

        self.selenium.get(response.url)

        mkt = CROMS_MKT_BY_RATE()
        l = ItemLoader( item = mkt, response = response )

        l.add_value('username', self.username)
        l.add_value('database', self.database)

        eRepoType = lambda: self.selenium.find_element_by_xpath( '//*[@id="ddlREPO_TYPE"]' )
        eSecurity = lambda: self.selenium.find_element_by_xpath( '//*[@id="drpBasket"]' )
        eSettType = lambda: self.selenium.find_element_by_xpath( '//*[@id="ddlSETT_TYPE"]' )
        eTenor = lambda: self.selenium.find_element_by_xpath( '//*[@id="ddlTenor"]' )
        eMaturity = lambda: self.selenium.find_element_by_xpath( '//*[@id="lblMaturity"]' )
        eGO = lambda: self.selenium.find_element_by_xpath( '//*[@id="btnGo"]' )

        eTimestamp = lambda: self.selenium.find_element_by_xpath( '//*[@id="lblTime"]' )
        eSummaryTable = lambda: self.selenium.find_elements_by_xpath( '//*[@id="grdMBR"]//tr' )
        eQuotesTable = lambda: self.selenium.find_elements_by_xpath( '//*[@id="grdMBR2"]//tr' )

        repoTypes = [ x.text for x in Select( eRepoType() ).options[1:] ]

        for i1 in repoTypes:

            Select( eRepoType() ).select_by_visible_text( i1 )

            if ( i1 == repoTypes[0] ):
                WebDriverWait( self.selenium, 5 ).until( EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="drpBasket"]' ), '' ) )
            else:
                WebDriverWait( self.selenium, 5 ).until( EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="lblMaturity"]' ), '--/--/----' ) )

            securities = [ x.text for x in Select( eSecurity() ).options[1:] ]

            for i2 in securities:

                Select( eSecurity() ).select_by_visible_text( i2 )

                if ( i2 == securities[0] ):
                    WebDriverWait( self.selenium, 5 ).until( EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="ddlSETT_TYPE"]' ), '' ) )
                else:
                    WebDriverWait( self.selenium, 5 ).until( \
                                EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="lblMaturity"]' ), '--/--/----' ) )

                settTypes = [ x.text for x in Select( eSettType() ).options[1:] ]

                for i3 in settTypes:

                    Select( eSettType() ).select_by_visible_text( i3 )

                    if ( i3 == settTypes[0] ):
                        WebDriverWait( self.selenium, 5 ).until( EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="ddlTenor"]' ), '' ) )
                    else:
                        WebDriverWait( self.selenium, 5 ).until(\
                                EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="lblMaturity"]' ), '--/--/----' ))

                    tenorTypes = [ x.text for x in Select( eTenor() ).options ]

                    for i4 in tenorTypes:

                        Select( eTenor() ).select_by_visible_text( i4 )

                        eGO().click()

                        try:
                            WebDriverWait( self.selenium, 5 ).until( \
                                            lambda x: x.find_elements_by_xpath('//*[@id="grdMBR"]//tr')[1].text.split(' ') != ['-']*8 \
                                            or x.find_elements_by_xpath('//*[@id="grdMBR2"]//tr')[1].text.split(' ')[-6:] != ['-']*6 )
                        except (TimeoutException):
                            continue

                        ts = eTimestamp().text
                        maturity = eMaturity().text

                        l.add_value( 'summary', [ sum( [ [sr_no, ts, i1, i2, i3, i4, maturity], eSummaryTable()[1].text.split(' ') ], [] ) ] )

                        qRows = eQuotesTable()[1:-2]

                        for row in qRows: l.add_value( 'quotes', [ sum( [ [sr_no, ts, i1, i2, i3, i4, maturity], row.text.split(' ')[1:] ], [] ) ] )

        l.load_item()
        self.__del__()
        return mkt

























