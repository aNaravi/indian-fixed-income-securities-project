from scrapy.spider import Spider
from scrapy import selector
from scrapy.item import Item, Field
from scrapy.contrib.loader import ItemLoader, processor
from ScrapeCCIL.items import OM_TRADES

from selenium import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium import common
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from selenium.webdriver.support.select import Select

from exceptions import IndexError, TypeError, ValueError
import psycopg2
import datetime as dt
import time

class om_trades(Spider):

    name = 'om_trades'
    start_urls = ['https://www.ccilindia.com/OMIT.aspx']

    def __init__(self, username, database, table='om_trades'):

        self.username, self.database, self.table = username, database, table

        self.con = psycopg2.connect(database = database, user = username)
        self.cur = self.con.cursor()
        self.cur.execute('''CREATE TABLE IF NOT EXISTS %s
		(
			ts timestamp without time zone NOT NULL,
            market_type character varying NOT NULL,
			sec_type character varying NOT NULL,
			sub_type character varying NOT NULL,
			security_name character varying NOT NULL,
			amount real NOT NULL,
			price real NOT NULL,
			yield real NOT NULL
		)''' % table)

        self.con.commit()

        Spider.__init__(self)
        self.selenium = webdriver.PhantomJS( executable_path='/home/akhil/Documents/Programming/WebDrivers/phantomjs',\
                                             service_args=['--ignore-ssl-errors=true'])


    def __del__(self):
        self.cur.close()
        self.con.close()
        self.selenium.quit()


    def read_table_rows(self, tradesTable, y, m, d, mkType, secType, subType, security, latest_ts):
        #print
        for row in tradesTable:
            try:
                row_ts = dt.datetime(y, m, d, *[int(x) for x in row.text.split(' ')[0].split(":")])
                lts_flag = row_ts <= latest_ts[0]

            except (TypeError):
                try:
                    if( row_ts > self.prev_ts ):
                        self.ts_flag = True
                        continue

                    elif( row_ts == self.prev_ts ):
                        if( self.ts_flag ):
                            del self.primary[:]
                            self.primary.append( [ sum( [[mkType, secType, subType, security], row.text.split(' ')], [] ) ] )
                            self.ts_flag = False
                        else:
                            self.primary.append( [ sum( [[mkType, secType, subType, security], row.text.split(' ')], [] ) ] )

                    elif( row_ts < self.prev_ts ):
                        for item in self.primary: self.l.add_value( 'trades', item )
                        del self.primary[:]
                        self.primary.append( [ sum( [[mkType, secType, subType, security], row.text.split(' ')], [] ) ] )
                        self.prev_ts = row_ts

                except (TypeError):
                    self.prev_ts = row_ts
                    self.primary.append( [ sum( [[mkType, secType, subType, security], row.text.split(' ')], [] ) ] )

            else:
                try:
                    if( row_ts > self.prev_ts ):
                        self.ts_flag = True
                        continue

                    elif( row_ts == self.prev_ts ):
                        if( self.ts_flag ):
                            if( lts_flag ):
                                del self.secondary[:]
                                self.ts_flag = False
                            else:
                                del self.primary[:]
                                self.primary.append( [ sum( [[mkType, secType, subType, security], row.text.split(' ')], [] ) ] )
                                self.ts_flag = False
                        elif( not(self.ts_flag) and not(lts_flag) ):
                            self.primary.append( [ sum( [[mkType, secType, subType, security], row.text.split(' ')], [] ) ] )

                    elif( row_ts < self.prev_ts ):
                        for item in self.primary: self.l.add_value( 'trades', item )#; print item
                        del self.primary[:]
                        self.prev_ts = row_ts
                        if( not(lts_flag) ): self.primary.append( [ sum( [[mkType, secType, subType, security], row.text.split(' ')], [] ) ] )

                    if( lts_flag == True ):
                        if( latest_ts[1] == None ):
                            return True

                        elif ( row_ts == latest_ts[1] ):
                            self.cur.execute("""SELECT COUNT(ts) FROM %s WHERE\
                                            sec_type = '%s' AND sub_type = '%s' AND security_name = '%s' AND ts > '%s' AND ts <= '%s' """\
                                            % (self.table, secType, subType, security, latest_ts[1], latest_ts[0]) )
                            count = self.cur.fetchone()[0]
                            for item in self.secondary[:-count]: self.l.add_value( 'trades', item )#; print item
                            del self.secondary[:]
                            return True

                        else:
                            self.secondary.append( [ sum( [[mkType, secType, subType, security], row.text.split(' ')], [] ) ] )
                            self.prev_ts = row_ts

                except (TypeError):
                    self.prev_ts = row_ts
                    if( not(lts_flag) ): self.primary.append( [ sum( [[mkType, secType, subType, security], row.text.split(' ')], [] ) ] )


        return False


    def traverse_pages(self, num_pages, tradesTable, y, m, d, mkType, secType, subType, security, lts, index=0):
        for i in range( num_pages ):
            pages = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')
            #print pages[i+index].text
            pages[i+index].click()
            if( self.read_table_rows( tradesTable(), y, m, d, mkType, secType, subType, security, lts ) ):
               return True

        return False


    def invisible_pages(self, link, tradesTable, y, m, d, mkType, secType, subType, security, lts):
        link.click()

        if( self.read_table_rows( tradesTable(), y, m, d, mkType, secType, subType, security, lts ) ):
            return

        inv_pages = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//*')
        index = 0
        while( inv_pages[index].tag_name != 'span' ):
            index+=1

        #print inv_pages[index].text
        inv_pages = lambda: self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')

        if (inv_pages()[-1].text == '...' ):
            if ( self.traverse_pages( len(inv_pages())-2, tradesTable, y, m, d, mkType, secType, subType, security, lts,  index ) ):#skip 2 '...' links
                return
            link = inv_pages()[-1]
            self.invisible_pages( link, tradesTable, y, m, d, mkType, secType, subType, security, lts)
        else:
            try:
                if ( self.traverse_pages( len(inv_pages())-1, tradesTable, y, m, d, mkType, secType, subType, security, lts, index) ):#skip '...' link
                    return
            except:#when index is out of range
                self.get_start_page()


    def get_start_page(self):
        pages = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//*')
        if ( pages[0].text == '...' ):
            pages[0].click()
            self.get_start_page()
        else:
            pages[0].click()
            return


    def parse(self, response):

        trades = OM_TRADES()
        self.l = ItemLoader( item = trades, response = response )

        self.l.add_value('username', self.username)
        self.l.add_value('database', self.database)
        self.l.add_value('table', self.table)

        self.selenium.get(response.url)
        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        xpathMarketType = '//*[@id="ddlMRKT_INDC"]'
        xpathSecType = '//*[@id="ddlISMT_INDC"]'
        xpathSubType = '//*[@id="ddlBOOK_INDC"]'
        xpathSecurities = '//*[@id="ddlISMT_IDNT"]'
        xpathTextField = '//*[@id="lblISMT_IDNT"]'
        xpathGO = '//*[@id="btnDisplay"]'

        eMarketType = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlMRKT_INDC"]')
        eSecTypes = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlISMT_INDC"]')
        eSubTypes = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlBOOK_INDC"]')
        eSecurities = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlISMT_IDNT"]')

        eGO = lambda: self.selenium.find_element_by_xpath('//*[@id="btnDisplay"]')

        eTradesTable = lambda: self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[1:-2]

        marketTypes = [ x.text for x in Select( eMarketType() ).options ]

        for i1 in marketTypes:

            Select( eMarketType() ).select_by_visible_text( i1 )

            WebDriverWait( self.selenium, 5 ).until( lambda x: x.find_element_by_xpath( xpathTextField ).text=='' )

            secTypes = [ x.text for x in Select( eSecTypes() ).options[1:] ]

            for i2 in secTypes:
                flag = 0

                Select( eSecTypes() ).select_by_visible_text( i2 )

                WebDriverWait(self.selenium, 5).until( lambda x: x.find_element_by_xpath( xpathTextField ).text=='' )

                if( i2 != secTypes[0] ):
                    try:
                        eGO().click()
                        if( not( eGO().is_enabled() ) ): flag = 1
                    except:
                        flag = 1 #NO SECURITY IN PREV ODD LOT

                subTypes = [ x.text for x in Select( eSubTypes() ).options ]

                if( i1 == marketTypes[1] ): subTypes.pop()

                for i3 in subTypes:

                    Select( eSubTypes() ).select_by_visible_text( i3 )

                    WebDriverWait(self.selenium, 5).until( lambda x: x.find_element_by_xpath( xpathTextField ).text=='' )

                    if ( flag == 1 ):
                        try:
                            WebDriverWait(self.selenium, 5).until_not( lambda x: x.find_element_by_xpath( xpathSecurities ).text=='' )
                            flag = 0
                        except (TimeoutException):
                            continue
                        except (StaleElementReferenceException):
                            pass

                    securities = [ x.text for x in Select( eSecurities() ).options ]

                    for i4 in securities:

                        Select( eSecurities() ).select_by_visible_text( i4 )

                        eGO().click()

                        try:
                            WebDriverWait(self.selenium, 7).until( EC.text_to_be_present_in_element( (By.XPATH, xpathTextField ), i4 ) )
                        except:
                            print 'TimeoutException: ', i1, i2, i3, i4
                            continue

                        latest_ts = []

                        self.cur.execute("""SELECT MAX(ts) FROM %s WHERE\
                                        sec_type = '%s' AND sub_type = '%s' and security_name = '%s'""" % (self.table, i2,i3,i4) )
                        latest_ts.append( self.cur.fetchone()[0] )
                        if ( latest_ts[0] == None ):
                            latest_ts = [None, None]
                        elif( latest_ts[0].date() < today.date() ):
                            latest_ts = [None, None]
                        else:
                            self.cur.execute("""SELECT MAX(ts) FROM %s WHERE\
                                            sec_type = '%s' AND sub_type = '%s' AND security_name = '%s' AND ts < '%s'""" \
                                             % (self.table, i2,i3,i4,latest_ts[0]) )
                            latest_ts.append( self.cur.fetchone()[0] )
                            if ( latest_ts[1] != None and latest_ts[1].date() < dt.datetime.today().date() ): latest_ts[1] = None

                        #print '\n'
                        #print i1, i2, i3, i4, latest_ts
                        #read first page
                        self.ts_flag, self.prev_ts, self.primary, self.secondary = False, 0, [], []

                        if( self.read_table_rows( eTradesTable(), y, m, d, i1, i2, i3, i4, latest_ts ) ):
                            continue

                        #links to other pages
                        try:
                            pages = lambda: self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')

                            if ( pages()[-1].text == "..." ):
                                if ( self.traverse_pages( len(pages())-1, eTradesTable, y, m, d, mkType=i1, secType=i2, subType=i3, security=i4, lts=latest_ts ) ):
                                    self.get_start_page()
                                    continue
                                link = pages()[-1]
                                self.invisible_pages( link, eTradesTable, y, m, d, mkType=i1, secType=i2, subType=i3, security=i4, lts=latest_ts )
                                for item in self.primary: self.l.add_value( 'trades', item )
                                del self.primary[:]
                                self.get_start_page()
                            else:
                                if ( self.traverse_pages( len(pages()), eTradesTable, y, m, d, mkType=i1, secType=i2, subType=i3, security=i4, lts=latest_ts ) ):
                                    self.get_start_page()
                                    continue
                                for item in self.primary: self.l.add_value( 'trades', item )
                                del self.primary[:]
                                self.get_start_page()
                        except (IndexError) :
                            for item in self.primary: self.l.add_value( 'trades', item )
                            del self.primary[:]

        self.l.load_item()
        self.__del__()
        return trades
