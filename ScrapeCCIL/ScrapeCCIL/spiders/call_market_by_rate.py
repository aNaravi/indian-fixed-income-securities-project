from scrapy.spider import Spider
from scrapy import selector
from scrapy.item import Item, Field
from scrapy.contrib.loader import ItemLoader, processor
from ScrapeCCIL.items import CALL_MKT_BY_RATE

from selenium import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium import common
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.support.select import Select

from exceptions import IndexError, TypeError
import psycopg2
import datetime as dt

class call_market_by_rate(Spider):

    name = 'call_mkt_by_rate'
    start_urls = ['https://www.ccilindia.com/CallMBR.aspx']

    def __init__(self, username, database):

        self.username, self.database = username, database

        self.con = psycopg2.connect(database = database, user = username)
        self.cur = self.con.cursor()
        self.cur.execute('''CREATE TABLE IF NOT EXISTS call_summary
        (
 			sr_no integer,
			ts timestamp without time zone NOT NULL,
			sett_type character varying NOT NULL,
			contract_type character varying NOT NULL,
			tenor character varying NOT NULL,
            maturity_date date NOT NULL,
 			open real NOT NULL,
			high real NOT NULL,
			low real NOT NULL,
			last_trade_rate real NOT NULL,
			last_trade_amount real NOT NULL,
			last_trade_timestamp time without time zone NOT NULL,
			total_traded_amount real NOT NULL,
			weighted_avg_rate real NOT NULL
		)''')

        self.con.commit()

        self.cur.execute('''CREATE TABLE IF NOT EXISTS call_quotes
        (
			sr_no integer NOT NULL,
			ts timestamp without time zone NOT NULL,
			sett_type character varying NOT NULL,
			contract_type character varying NOT NULL,
			tenor character varying NOT NULL,
            maturity_date date NOT NULL,
			borrow_amt real,
			borrow_rate real,
			lend_rate real,
			lend_amt real
		)''')

        self.con.commit()

        Spider.__init__(self)

        self.selenium = webdriver.PhantomJS( executable_path='/home/akhil/Documents/Programming/WebDrivers/phantomjs',\
                                             service_args=['--ignore-ssl-errors=true'])


    def __del__(self):
        self.cur.close()
        self.con.close()
        self.selenium.quit()


    def parse(self, response):

        self.cur.execute('''select max(sr_no) from call_quotes where ts >= %s''', [dt.datetime.date(dt.datetime.today())] )
        try:
            sr_no = self.cur.fetchone()[0] + 1
        except (TypeError):
            sr_no = 1

        mkt =  CALL_MKT_BY_RATE()
        self.l = ItemLoader( item = mkt, response = response )

        self.l.add_value('username', self.username)
        self.l.add_value('database', self.database)

        self.selenium.get(response.url)

        xpathTextField = '//*[@id="lblISMT_IDNT"]'
        xpathMaturityField = '//*[@id="lblMaturity"]'

        eSettTypes = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlSTLM_INDC"]')
        eContractTypes = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlISMT_INDC"]')
        eTenorTypes = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlISMT_IDNT"]')

        eGO = lambda: self.selenium.find_element_by_xpath('//*[@id="btnGo"]')

        eMaturity = lambda: self.selenium.find_element_by_xpath('//*[@id="lblMaturity"]')
        eTimestamp = lambda: self.selenium.find_element_by_xpath('//*[@id="lblTime"]')
        eSummaryTable = lambda: self.selenium.find_elements_by_xpath('//*[@id="grdMBR"]//tr')
        eQuotesTable = lambda: self.selenium.find_elements_by_xpath('//*[@id="grdMBR2"]/tbody//tr')[1:-2]

        settTypes = [ x.text for x in Select( eSettTypes() ).options[1:] ]

        maturityField = eMaturity().text

        for i1 in settTypes:

            Select( eSettTypes() ).select_by_visible_text( i1 )

            if ( i1 == settTypes[1] ):
                try:
                    WebDriverWait(self.selenium, 5).until_not( lambda x: x.find_element_by_xpath( xpathMaturityField ).text == maturityField )
                    maturityField = eMaturity().text
                except (TypeError, TimeoutException):
                    pass #CALL - continue to check other contract types

            contractTypes = [ x.text for x in Select( eContractTypes() ).options ]

            for i2 in contractTypes:

                Select( eContractTypes() ).select_by_visible_text( i2 )

                try:
                    WebDriverWait(self.selenium, 5).until_not( lambda x: x.find_element_by_xpath( xpathMaturityField ).text == maturityField )
                    maturityField = self.selenium.find_element_by_xpath( xpathMaturityField ).text
                except ( TypeError, NoSuchElementException ):
                    continue
                except ( TimeoutException ):
                    if ( maturityField == None ):
                        continue

                tenorTypes = [ x.text for x in Select( eTenorTypes() ).options ]

                for i3 in tenorTypes:

                    Select( eTenorTypes() ).select_by_visible_text( i3 )

                    eGO().click()

                    if ( i2 == contractTypes[1] or i2 == contractTypes[2] ):
                        s = i2 + " Money: " + ''.join(i3.split(' ')) + " (" + ''.join(i1.split(' ')) + ")"
                    elif ( i2 == contractTypes[0] ):
                        s = i2 + " Money (" + ''.join(i1.split(' ')) + ")"

                    try:
                        WebDriverWait(self.selenium, 5).until( EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="lblISMT_IDNT"]' ), s ) )
                    except ( TimeoutException ):
                        continue

                    self.l.add_value( 'summary', \
                            [ sum( [[sr_no, eTimestamp().text, i1, i2, i3, eMaturity().text], eSummaryTable()[1].text.split(' ') ], [] ) ] )

                    for r in eQuotesTable():
                        self.l.add_value( 'quotes', [ sum( [[sr_no, eTimestamp().text, i1, i2, i3, eMaturity().text], r.text.split(' ')[1:] ], [] ) ] )

        self.l.load_item()
        self.__del__()
        return mkt
