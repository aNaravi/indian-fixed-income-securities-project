from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy.contrib.loader import ItemLoader
from scrapy.http import Request
from ScrapeCCIL.items import FOREX

import psycopg2
import re
import datetime as dt

class forex(Spider):

    name = 'forex'
    start_urls = ['https://www.ccilindia.com/_Layouts/MarketToday/CBLOForexData.aspx?PageType=ForexMWatch']
    forex = FOREX()
    l = ItemLoader( item = forex )

    def __init__(self, username, database):
        self.con = psycopg2.connect(database = database, user = username)
        self.cur = self.con.cursor()

        self.cur.execute('''CREATE TABLE IF NOT EXISTS forex_spot_summary
        (
            sr_no integer NOT NULL,
            ts timestamp without time zone NOT NULL,
            open real NOT NULL,
            high real NOT NULL,
            low real NOT NULL,
            close real NOT NULL
        )''')

        self.cur.execute('''CREATE TABLE IF NOT EXISTS forex_spot_quotes
        (
            sr_no integer NOT NULL,
            ts timestamp without time zone NOT NULL,
            bid_orders integer,
            bid_quantity real,
            bid_price real,
            offer_price real,
            offer_quantity real,
            offer_orders integer
        )''')

        self.con.commit()

        self.cur.execute('''select max(sr_no) from forex_spot_quotes where ts >= %s''', [dt.datetime.date(dt.datetime.today())] )
        try:
            self.sr_no = self.cur.fetchone()[0] + 1
        except (TypeError):
            self.sr_no = 1

        self.l.add_value('username', username)
        self.l.add_value('database', database)
        self.RE = re.compile('[^\\r\\n].*')

        Spider.__init__(self)


    def __del__(self):
        self.cur.close()
        self.con.close()


    def parse(self, response):


        sel = Selector(response)

        ts = sel.xpath('//*[@id="lblForexDateTime"]//text()').extract()[0]

        summary = filter(self.RE.match, sel.xpath('//*[@id="tblWatch"]//tr')[2].xpath('.//text()').extract())[1:]
        self.l.add_value( 'summary', [ sum( [ [self.sr_no, ts], summary ], [] ) ] )

        bidTable = sel.xpath('//*[@id="tblForexBuy"]//tr')[2:]
        offerTable = sel.xpath('//*[@id="tblForexSell"]//tr')[2:]

        for b,o in zip(bidTable, offerTable):
            self.l.add_value( 'quotes', \
            [ sum( [ [self.sr_no, ts], filter(self.RE.match, b.xpath('.//text()').extract()), filter(self.RE.match, o.xpath('.//text()').extract()) ], [] ) ] )

        self.l.load_item()
        self.__del__()
        return self.forex
