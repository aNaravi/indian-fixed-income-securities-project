from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy.item import Item, Field
from scrapy.contrib.loader import ItemLoader
from scrapy.http import Request
from ScrapeCCIL.items import DERIVATIVES

import psycopg2
import datetime as dt
import re

class derivatives(Spider):

    name = 'derivatives'
    start_urls = ['https://www.ccilindia.com/IRSS_HOME.aspx']
    derivatives = DERIVATIVES()
    l = ItemLoader( item = derivatives )

    def __init__(self, username, database):

        self.con = psycopg2.connect(database = database, user = username)
        self.cur = self.con.cursor()

        self.cur.execute('''CREATE TABLE IF NOT EXISTS irs_ff
        (
 			sr_no integer NOT NULL,
            benchmark character varying NOT NULL,
			ts timestamp without time zone NOT NULL,
            maturity character varying NOT NULL,
 			last_reported_prev real,
			high real,
			low real,
			wt_avg real,
			last_reported_curr real,
			volume real,
			trades integer
		)''')

        self.cur.execute('''CREATE TABLE IF NOT EXISTS fra
        (
 			sr_no integer NOT NULL,
            benchmark character varying NOT NULL,
			ts timestamp without time zone NOT NULL,
            tenor character varying NOT NULL,
 			last_reported_prev real,
			high real,
			low real,
			wt_avg real,
			last_reported_curr real,
			volume real,
			trades integer
		)''')

        self.con.commit()

        self.cur.execute('''select max(sr_no) from irs_ff where ts >= %s''', [dt.datetime.date(dt.datetime.today())] )
        try:
            self.sr_no = self.cur.fetchone()[0] + 1
        except (TypeError):
            self.sr_no = 1

        self.l.add_value('username', username)
        self.l.add_value('database', database)

        self.RE = re.compile('[^\\r\\n].*')

        Spider.__init__(self)


    def __del__(self):
        self.cur.close()
        self.con.close()


    def make_requests_from_url(self, url):

        return {
            'https://www.ccilindia.com/IRSS_HOME.aspx':Request( url, callback=self.parse_irs_ff_home ),
            'https://www.ccilindia.com/IRSS_OTHR.aspx?DTLS=MBOR':Request( url, callback=self.parse_irs_ff_mibor ),
            'https://www.ccilindia.com/IRSS_OTHR.aspx?DTLS=INBK':Request( url, callback=self.parse_irs_ff_inbmk ),
            'https://www.ccilindia.com/IRSS_OTHR.aspx?DTLS=MIOS':Request( url, callback=self.parse_irs_ff_miois ),
            'https://www.ccilindia.com/IRSS_FRAA.aspx':Request( url, callback=self.parse_fra )
        }[url]


    def parse_irs_ff_home(self, response):
        sel = Selector(response)

        ts = sel.xpath('//*[@id="lblTIME"]//text()')[0].extract()

        #MIBOR
        table = sel.xpath('//*[@id="grdIRSS_DTLS"]//tr')[1:-1]

        for row in table:
            row = filter(self.RE.match, [ x if x != u'\xa0' else ' ' for x in row.xpath('.//text()').extract() ])
            self.l.add_value( 'irs_ff', [ sum( [ [self.sr_no, 'MIBOR OIS', ts], row], [] ) ] )

        #MIFOR
        table = sel.xpath('//*[@id="grdIRSS_DTLS2"]//tr')[1:-1]

        for row in table:
            row = filter(self.RE.match, [ x if x != u'\xa0' else ' ' for x in row.xpath('.//text()').extract() ])
            self.l.add_value( 'irs_ff', [ sum( [ [self.sr_no, 'MIFOR', ts], row], [] ) ] )

        yield self.make_requests_from_url( url = 'https://www.ccilindia.com/IRSS_OTHR.aspx?DTLS=MBOR' )


    def parse_irs_ff_mibor(self, response):
        sel = Selector(response)
        l = ItemLoader( item = self.derivatives, response = response )

        ts = sel.xpath('//*[@id="lblTIME"]//text()')[0].extract()
        #MIBOR OTHER THAN OIS
        table = sel.xpath('//*[@id="grdIRSS_OTHR"]//tr')[1:-1]

        for row in table:
            row = filter(self.RE.match, [ x if x != u'\xa0' else ' ' for x in row.xpath('.//text()').extract() ])
            self.l.add_value( 'irs_ff', [ sum( [ [self.sr_no, 'MIBOR', ts], row], [] ) ] )

        yield self.make_requests_from_url( url = 'https://www.ccilindia.com/IRSS_OTHR.aspx?DTLS=INBK' )


    def parse_irs_ff_inbmk(self, response):
        sel = Selector(response)
        l = ItemLoader( item = self.derivatives, response = response )

        ts = sel.xpath('//*[@id="lblTIME"]//text()')[0].extract()
        #MIBOR OTHER THAN OIS
        table = sel.xpath('//*[@id="grdIRSS_OTHR"]//tr')[1:-1]

        for row in table:
            row = filter(self.RE.match, [ x if x != u'\xa0' else ' ' for x in row.xpath('.//text()').extract() ])
            self.l.add_value( 'irs_ff', [ sum( [ [self.sr_no, 'INBMK', ts], row], [] ) ] )

        yield self.make_requests_from_url( url = 'https://www.ccilindia.com/IRSS_OTHR.aspx?DTLS=MIOS' )


    def parse_irs_ff_miois(self, response):
        sel = Selector(response)
        l = ItemLoader( item = self.derivatives, response = response )

        ts = sel.xpath('//*[@id="lblTIME"]//text()')[0].extract()
        #MIBOR OTHER THAN OIS
        table = sel.xpath('//*[@id="grdIRSS_OTHR"]//tr')[1:-1]

        for row in table:
            row = filter(self.RE.match, [ x if x != u'\xa0' else ' ' for x in row.xpath('.//text()').extract() ])
            self.l.add_value( 'irs_ff', [ sum( [ [self.sr_no, 'MIOIS', ts], row], [] ) ] )

        yield self.make_requests_from_url( url = 'https://www.ccilindia.com/IRSS_FRAA.aspx' )


    def parse_fra(self, response):
        sel = Selector(response)
        l = ItemLoader( item = self.derivatives, response = response )

        ts = sel.xpath('//*[@id="lblTIME"]//text()')[0].extract()

        #MIBOR
        table = sel.xpath('//*[@id="grdFRAA"]//tr')[1:-1]

        for row in table:
            row = filter(self.RE.match, [ x if x != u'\xa0' else ' ' for x in row.xpath('.//text()').extract() ])
            self.l.add_value( 'fra', [ sum( [ [self.sr_no, 'MIBOR', ts], row], [] ) ] )

        #MIFOR
        table = sel.xpath('//*[@id="grdFRMF"]//tr')[1:-1]

        for row in table:
            row = filter(self.RE.match, [ x if x != u'\xa0' else ' ' for x in row.xpath('.//text()').extract() ])
            self.l.add_value( 'fra', [ sum( [ [self.sr_no, 'MIFOR', ts], row], [] ) ] )

        self.l.load_item()
        self.__del__()
        return self.derivatives









