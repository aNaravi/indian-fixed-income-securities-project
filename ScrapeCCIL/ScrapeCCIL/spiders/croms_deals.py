from scrapy.spider import Spider
from scrapy import selector
from scrapy.item import Item, Field
from scrapy.contrib.loader import ItemLoader, processor
from ScrapeCCIL.items import CROMS_DEALS

from selenium import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium import common
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import *
from selenium.webdriver.support.select import Select

from exceptions import IndexError, TypeError
import psycopg2
import datetime as dt
import time

class croms_mkt_deals(Spider):

    name = 'croms_deals'
    start_urls = ['https://www.ccilindia.com/RepoHome.aspx']

    def __init__(self, username, database, table='croms_deals'):

        self.username, self.database, self.table = username, database, table

        self.con = psycopg2.connect(database = database, user = username)
        self.cur = self.con.cursor()
        self.cur.execute('''CREATE TABLE IF NOT EXISTS %s
		(
			ts timestamp without time zone NOT NULL,
			repo_type character varying NOT NULL,
			security_name character varying NOT NULL,
			sett_type character varying NOT NULL,
            tenor character varying NOT NULL,
			repo_maturity_date date NOT NULL,
			amount real,
			rate real,
            price real
		)''' % table)

        self.con.commit()

        Spider.__init__(self)

        self.selenium = webdriver.PhantomJS( executable_path='/home/akhil/Documents/Programming/WebDrivers/phantomjs',\
                                             service_args=['--ignore-ssl-errors=true'])


    def __del__(self):
        self.cur.close()
        self.con.close()
        self.selenium.quit()


    def read_table_rows(self, dealsTable, y, m, d, repoType, security, settType, tenor, repoMaturity, latest_ts):
        #print
        for row in dealsTable:
            try:
                row_ts = dt.datetime(y, m, d, *[int(x) for x in row.text.split(' ')[0].split(":")])
                lts_flag = row_ts <= latest_ts[0]

            except (TypeError):
                try:
                    if( row_ts > self.prev_ts ):
                        self.ts_flag = True
                        continue

                    elif( row_ts == self.prev_ts ):
                        if( self.ts_flag ):
                            del self.primary[:]
                            self.primary.append( [ sum( [[repoType, security, settType, tenor, repoMaturity], row.text.split(' ')], [] ) ] )
                            self.ts_flag = False
                        else:
                            self.primary.append( [ sum( [[repoType, security, settType, tenor, repoMaturity], row.text.split(' ')], [] ) ] )

                    elif( row_ts < self.prev_ts ):
                        for item in self.primary: self.l.add_value( 'deals', item )
                        del self.primary[:]
                        self.primary.append( [ sum( [[repoType, security, settType, tenor, repoMaturity], row.text.split(' ')], [] ) ] )
                        self.prev_ts = row_ts

                except (TypeError):
                    self.prev_ts = row_ts
                    self.primary.append( [ sum( [[repoType, security, settType, tenor, repoMaturity], row.text.split(' ')], [] ) ] )

            else:
                try:
                    if( row_ts > self.prev_ts ):
                        self.ts_flag = True
                        continue

                    elif( row_ts == self.prev_ts ):
                        if( self.ts_flag ):
                            if( lts_flag ):
                                del self.secondary[:]
                                self.ts_flag = False
                            else:
                                del self.primary[:]
                                self.primary.append( [ sum( [[repoType, security, settType, tenor, repoMaturity], row.text.split(' ')], [] ) ] )
                                self.ts_flag = False
                        elif( not(self.ts_flag) and not(lts_flag) ):
                            self.primary.append( [ sum( [[repoType, security, settType, tenor, repoMaturity], row.text.split(' ')], [] ) ] )

                    elif( row_ts < self.prev_ts ):
                        for item in self.primary: self.l.add_value( 'deals', item )#; print item
                        del self.primary[:]
                        self.prev_ts = row_ts
                        if( not(lts_flag) ): self.primary.append( [ sum( [[repoType, security, settType, tenor, repoMaturity], row.text.split(' ')], [] ) ] )

                    if( lts_flag == True ):
                        if( latest_ts[1] == None ):
                            return True

                        elif ( row_ts == latest_ts[1] ):
                            self.cur.execute("""SELECT COUNT(ts) FROM %s WHERE\
                                             repo_type = '%s' AND security_name = '%s' AND sett_type = '%s' AND tenor = '%s' \
                                             AND repo_maturity_date = '%s' AND ts > '%s' AND ts <= '%s' """\
                                            % (self.table, repoType, security, settType, tenor, repoMaturity, latest_ts[1], latest_ts[0]) )
                            count = self.cur.fetchone()[0]
                            for item in self.secondary[:-count]: self.l.add_value( 'deals', item )#; print item
                            del self.secondary[:]
                            return True

                        else:
                            self.secondary.append( [ sum( [[repoType, security, settType, tenor, repoMaturity], row.text.split(' ')], [] ) ] )
                            self.prev_ts = row_ts

                except (TypeError):
                    self.prev_ts = row_ts
                    if( not(lts_flag) ): self.primary.append( [ sum( [[repoType, security, settType, tenor, repoMaturity], row.text.split(' ')], [] ) ] )

        return False


    def traverse_pages(self, num_pages, eDealsTable, y, m, d, repoType, security, settType, tenor, repoMaturity, lts, index=0):
        for i in range( num_pages ):
            pages = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')
            #print pages[i+index].text
            pages[i+index].click()
            if( self.read_table_rows( eDealsTable(), y, m, d, repoType, security, settType, tenor, repoMaturity, lts ) ):
               return True

        return False


    def invisible_pages(self, link, eDealsTable, y, m, d, repoType, security, settType, tenor, repoMaturity, lts):
        link.click()
        trades_rows = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[1:-2]
        if( self.read_table_rows( eDealsTable(), y, m, d, repoType, security, settType, tenor, repoMaturity, lts ) ):
            return

        inv_pages = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//*')
        index = 0
        while( inv_pages[index].tag_name != 'span' ):
            index+=1

        #print inv_pages[index].text
        inv_pages = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')

        if (inv_pages[-1].text == '...' ):
            if ( self.traverse_pages( len(inv_pages)-2, eDealsTable, y, m, d, repoType, security, settType, tenor, repoMaturity, lts,  index ) ):#skip 2 '...' links
                return
            link = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')[-1]
            self.invisible_pages( link, eDealsTable, y, m, d, repoType, security, settType, tenor, repoMaturity, lts)
        else:
            try:
                if ( self.traverse_pages( len(inv_pages)-1, eDealsTable, y, m, d, repoType, security, settType, tenor, repoMaturity, lts, index) ):#skip '...' link
                    return
            except:#when index is out of range
                self.get_start_page()


    def get_start_page(self):
        pages = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//*')
        if ( pages[0].text == '...' ):
            pages[0].click()
            self.get_start_page()
        else:
            pages[0].click()
            return


    def parse_trades(self, eLink, repoType, security, tenor, settType, repoMaturity, y, m, d):
        eLink().click()
        eTradesTable = lambda: self.selenium.find_elements_by_xpath('//*[@id="grdIT"]//tr')[1:-2]

        latest_ts = []
        self.cur.execute("""SELECT MAX(ts) FROM %s WHERE\
                        repo_type = '%s' AND security_name = '%s' AND tenor = '%s' AND sett_type = '%s' AND repo_maturity_date = '%s'""" \
                        % (self.table, repoType, security, tenor, settType, repoMaturity) )
        latest_ts.append( self.cur.fetchone()[0] )
        if ( latest_ts[0] == None ):
            latest_ts = [None, None]
        elif( latest_ts[0].date() < dt.date(y, m, d) ):
            latest_ts = [None, None]
        else:
            self.cur.execute("""SELECT MAX(ts) FROM %s WHERE\
                repo_type = '%s' AND security_name = '%s' AND tenor = '%s' AND sett_type = '%s' AND repo_maturity_date = '%s' AND ts < '%s'""" \
                            % (self.table, repoType, security, tenor, settType, repoMaturity, latest_ts[0]) )
            latest_ts.append( self.cur.fetchone()[0] )
            if ( latest_ts[1] != None and latest_ts[1].date() < dt.date(y, m, d) ): latest_ts[1] = None

        #read first page
        self.ts_flag, self.prev_ts, self.primary, self.secondary = False, 0, [], []
        #print security, tenor, settType, repoMaturity, latest_ts
        if( self.read_table_rows( eTradesTable(), y, m, d, repoType, security, settType, tenor, repoMaturity, latest_ts ) ): return

        #links to other pages
        try:
            pages = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')
            #print len(pages)+1

            if ( pages[-1].text == "..." ):
                if ( self.traverse_pages( len(pages)-1, eTradesTable, y, m, d, repoType, security, settType, tenor, repoMaturity, lts=latest_ts ) ):
                    self.get_start_page()
                    return
                link = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')[-1]
                self.invisible_pages( link, eTradesTable, y, m, d, repoType, security, settType, tenor, repoMaturity, lts=latest_ts )
                for item in self.primary: self.l.add_value( 'deals', item )
                del self.primary[:]
                self.get_start_page()
            else:
                if ( self.traverse_pages( len(pages), eTradesTable, y, m, d, repoType, security, settType, tenor, repoMaturity, lts=latest_ts ) ):
                    self.get_start_page()
                    return
                for item in self.primary: self.l.add_value( 'deals', item )
                del self.primary[:]
                self.get_start_page()
        except (IndexError) :
            for item in self.primary: self.l.add_value( 'deals', item )
            del self.primary[:]


    def parse_table(self, eTable, y, m, d, repo_type):
        ePages = lambda: eTable()[-1].find_elements_by_xpath('.//*')[1:]

        if( repo_type == 'Basket' ):
            for p in range( len( ePages() ) ):
                #print "Page: ", ePages()[p].text
                try:
                    ePages()[p].click()
                except (ElementNotVisibleException):
                    for row in range(1, len(eTable()[1:-1]) ):
                        security, trades, sett_type, tenor, repo_maturity, tta, tta_fv, open_p, high_p, low_p, ltr, war = [x.text for x in eTable()[row].find_elements_by_xpath('.//td')]
                        indv_trades_link = lambda: eTable()[row].find_elements_by_xpath('.//a')[1]
                        repo_maturity = dt.datetime( *( [ int(x) for x in repo_maturity.split('/')[::-1] ] ) ).date()
                        self.parse_trades( indv_trades_link, repo_type, security, tenor, sett_type, repo_maturity, y, m, d)
                        self.selenium.find_element_by_xpath('/html/body/form/table[4]/tbody/tr[2]/td[1]/a').click()
                    return
                else:
                    for row in range(1, len(eTable()[1:-1]) ):
                        security, trades, sett_type, tenor, repo_maturity, tta, tta_fv, open_p, high_p, low_p, ltr, war = [x.text for x in eTable()[row].find_elements_by_xpath('.//td')]
                        indv_trades_link = lambda: eTable()[row].find_elements_by_xpath('.//a')[1]
                        repo_maturity = dt.datetime( *( [ int(x) for x in repo_maturity.split('/')[::-1] ] ) ).date()
                        self.parse_trades( indv_trades_link, repo_type, security, tenor, sett_type, repo_maturity, y, m, d)
                        self.selenium.find_element_by_xpath('/html/body/form/table[4]/tbody/tr[2]/td[1]/a').click()
                        ePages()[p].click()

        else:
            for p in range( len( ePages() ) ):
                #print "Page: ", ePages()[p].text
                try:
                    ePages()[p].click()
                except (ElementNotVisibleException):
                    for row in range(1, len(eTable()[1:-1]) ):
                        security, maturity, trades, sett_type, tenor, repo_maturity, tta, tta_mv, open_p, high_p, low_p, ltr, war = [x.text for x in eTable()[row].find_elements_by_xpath('.//td')]
                        indv_trades_link = lambda: eTable()[row].find_elements_by_xpath('.//a')[1]
                        repo_maturity = dt.datetime( *( [ int(x) for x in repo_maturity.split('/')[::-1] ] ) ).date()
                        self.parse_trades( indv_trades_link, repo_type, security, tenor, sett_type, repo_maturity, y, m, d)
                        self.selenium.find_element_by_xpath('/html/body/form/table[4]/tbody/tr[2]/td[1]/a').click()
                    return
                else:
                    for row in range(1, len(eTable()[1:-1]) ):
                        security, maturity, trades, sett_type, tenor, repo_maturity, tta, tta_mv, open_p, high_p, low_p, ltr, war = [x.text for x in eTable()[row].find_elements_by_xpath('.//td')]
                        indv_trades_link = lambda: eTable()[row].find_elements_by_xpath('.//a')[1]
                        repo_maturity = dt.datetime( *( [ int(x) for x in repo_maturity.split('/')[::-1] ] ) ).date()
                        self.parse_trades( indv_trades_link, repo_type, security, tenor, sett_type, repo_maturity, y, m, d)
                        self.selenium.find_element_by_xpath('/html/body/form/table[4]/tbody/tr[2]/td[1]/a').click()
                        ePages()[p].click()


    def parse(self, response):

        deals = CROMS_DEALS()
        self.l = ItemLoader( item = deals, response = response )

        self.l.add_value('username', self.username)
        self.l.add_value('database', self.database)
        self.l.add_value('table', self.table)

        self.selenium.get(response.url)
        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        eBasketRepoTable = lambda: self.selenium.find_elements_by_xpath('//*[@id="grdNDSOMReg"]//tr')
        eSpecialRepoTable = lambda: self.selenium.find_elements_by_xpath('//*[@id="grdNDSOMSP"]//tr')

        self.parse_table( eBasketRepoTable, y, m, d, 'Basket')
        self.parse_table( eSpecialRepoTable, y, m, d, 'Special')

        self.l.load_item()
        self.__del__()
        return deals


'''
    def parse(self, response):

        deals = CROMS_DEALS()
        self.l = ItemLoader( item = deals, response = response )

        self.l.add_value('username', self.username)
        self.l.add_value('database', self.database)
        self.l.add_value('table', self.table)

        today = dt.datetime.today().date()
        y, m, d = today.year, today.month, today.day

        eRepoType = lambda: self.selenium.find_element_by_xpath( '//*[@id="ddlREPO_TYPE"]' )
        eSecurity = lambda: self.selenium.find_element_by_xpath( '//*[@id="drpBasket"]' )
        eSettType = lambda: self.selenium.find_element_by_xpath( '//*[@id="ddlSETT_TYPE"]' )
        eTenor = lambda: self.selenium.find_element_by_xpath( '//*[@id="ddlTenor"]' )
        eMaturity = lambda: self.selenium.find_element_by_xpath( '//*[@id="lblMaturity"]' )
        eGO = lambda: self.selenium.find_element_by_xpath( '//*[@id="btnGo"]' )

        eDealsTable = lambda: self.selenium.find_elements_by_xpath( '//*[@id="grdIT"]//tr' )[1:-2]

        self.selenium.get(response.url)

        repoTypes = [ x.text for x in Select( eRepoType() ).options[1:] ]

        for i1 in repoTypes:

            Select( eRepoType() ).select_by_visible_text( i1 )

            if ( i1 == repoTypes[0] ):
                WebDriverWait( self.selenium, 5 ).until( EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="drpBasket"]' ), '' ) )
            else:
                WebDriverWait( self.selenium, 5 ).until( EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="lblMaturity"]' ), '--/--/----' ) )

            securities = [ x.text for x in Select( eSecurity() ).options[1:] ]

            for i2 in securities:

                Select( eSecurity() ).select_by_visible_text( i2 )

                if ( i2 == securities[0] ):
                    WebDriverWait( self.selenium, 5 ).until( EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="ddlSETT_TYPE"]' ), '' ) )
                else:
                    WebDriverWait( self.selenium, 5 ).until( \
                                EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="lblMaturity"]' ), '--/--/----' ) )

                settTypes = [ x.text for x in Select( eSettType() ).options[1:] ]

                for i3 in settTypes:

                    Select( eSettType() ).select_by_visible_text( i3 )

                    if ( i3 == settTypes[0] ):
                        WebDriverWait( self.selenium, 5 ).until( EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="ddlTenor"]' ), '' ) )

                    tenorTypes = [ x.text for x in Select( eTenor() ).options ]

                    for i4 in tenorTypes:

                        Select( eTenor() ).select_by_visible_text( i4 )

                        eGO().click()

                        latest_ts = []

                        self.cur.execute("""SELECT MAX(ts) FROM %s WHERE \
                                         repo_type = '%s' AND security_name = '%s' AND sett_type = '%s' AND tenor = '%s' """\
                                         % (self.table, i1, i2, i3, i4) )
                        latest_ts.append( self.cur.fetchone()[0] )
                        if ( latest_ts[0] == None ):
                            latest_ts = [None, None]
                        elif( latest_ts[0].date() < today ):
                            latest_ts = [None, None]
                        else:
                            self.cur.execute("""SELECT MAX(ts) FROM %s WHERE \
                                         repo_type = '%s' AND security_name = '%s' AND sett_type = '%s' AND tenor = '%s' AND ts < '%s' """\
                                         % (self.table, i1, i2, i3, i4, latest_ts[0]) )
                            latest_ts.append( self.cur.fetchone()[0] )
                            if ( latest_ts[1] != None and latest_ts[1].date() < today ): latest_ts[1] = None

                        #print i1, i2, i3, i4, latest_ts
                        self.ts_flag, self.prev_ts, self.primary, self.secondary = False, 0, [], []

                        if( self.read_table_rows( eDealsTable(), y, m, d, i1, i2, i3, i4, eMaturity().text, latest_ts ) ):
                            continue

                        #links to other pages
                        try:
                            pages = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')
                            if( not( any( map( lambda x: x.is_displayed(), pages ) ) ) == True ) : continue

                            if ( pages[-1].text == "..." ):
                                if ( self.traverse_pages( len(pages)-1, eDealsTable, y, m, d, repoType=i1, security=i2, settType=i3, tenor=i4, maturity=eMaturity().text, lts=latest_ts ) ):
                                    self.get_start_page()
                                    continue
                                link = self.selenium.find_elements_by_xpath('//*[@id="grdIT"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')[-1]
                                self.invisible_pages( link, eDealsTable, y, m, d, repoType=i1, security=i2, settType=i3, tenor=i4, maturity=eMaturity().text, lts=latest_ts )
                                for item in self.primary: self.l.add_value( 'deals', item )
                                del self.primary[:]
                                self.get_start_page()
                            else:
                                if ( self.traverse_pages( len(pages), eDealsTable, y, m, d, repoType=i1, security=i2, settType=i3, tenor=i4, maturity=eMaturity().text, lts=latest_ts ) ):
                                    self.get_start_page()
                                    continue
                                for item in self.primary: self.l.add_value( 'deals', item )
                                del self.primary[:]
                                self.get_start_page()
                        except (IndexError) :
                            for item in self.primary: self.l.add_value( 'deals', item )
                            del self.primary[:]

        self.l.load_item()
        self.__del__()
        return deals


'''
