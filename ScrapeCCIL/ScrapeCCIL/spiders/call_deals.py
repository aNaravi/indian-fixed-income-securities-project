from scrapy.spider import Spider
from scrapy import selector
from scrapy.item import Item, Field
from scrapy.contrib.loader import ItemLoader, processor
from ScrapeCCIL.items import CALL_DEALS

from selenium import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium import common
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException, InvalidElementStateException
from selenium.webdriver.support.select import Select

from exceptions import IndexError, TypeError
import psycopg2
import datetime as dt

class call_individual_deals(Spider):

    name = 'call_deals'
    start_urls = ['https://www.ccilindia.com/CallDeal.aspx']

    def __init__(self, username, database, table='call_deals'):

        self.username, self.database, self.table = username, database, table

        self.con = psycopg2.connect(database = database, user = username)
        self.cur = self.con.cursor()
        self.cur.execute('''CREATE TABLE IF NOT EXISTS %s
		(
			ts timestamp without time zone NOT NULL,
			sett_type character varying NOT NULL,
			contract_type character varying NOT NULL,
			tenor character varying NOT NULL,
			maturity_date date NOT NULL,
			amount real NOT NULL,
			rate real NOT NULL
		)''' % table)

        self.con.commit()

        Spider.__init__(self)

        self.selenium = webdriver.PhantomJS( executable_path='/home/akhil/Documents/Programming/WebDrivers/phantomjs',\
                                             service_args=['--ignore-ssl-errors=true'])


    def __del__(self):
        self.cur.close()
        self.con.close()
        self.selenium.quit()


    def read_table_rows(self, dealsTable, y, m, d, settType, contractType, tenorType, maturity, latest_ts):
        #print
        for row in dealsTable:
            try:
                try:
                    row_ts = dt.datetime(y, m, d, *[int(x) for x in row.text.split(' ')[0].split(":")])
                except:
                    print tenorType
                    return True
                lts_flag = row_ts <= latest_ts[0]

            except (TypeError):
                try:
                    if( row_ts > self.prev_ts ):
                        self.ts_flag = True
                        continue

                    elif( row_ts == self.prev_ts ):
                        if( self.ts_flag ):
                            del self.primary[:]
                            self.primary.append( [ sum( [[settType, contractType, tenorType, maturity], row.text.split(' ')], [] ) ] )
                            self.ts_flag = False
                        else:
                            self.primary.append( [ sum( [[settType, contractType, tenorType, maturity], row.text.split(' ')], [] ) ] )

                    elif( row_ts < self.prev_ts ):
                        for item in self.primary: self.l.add_value( 'deals', item )
                        del self.primary[:]
                        self.primary.append( [ sum( [[settType, contractType, tenorType, maturity], row.text.split(' ')], [] ) ] )
                        self.prev_ts = row_ts

                except (TypeError):
                    self.prev_ts = row_ts
                    self.primary.append( [ sum( [[settType, contractType, tenorType, maturity], row.text.split(' ')], [] ) ] )

            else:
                try:
                    if( row_ts > self.prev_ts ):
                        self.ts_flag = True
                        continue

                    elif( row_ts == self.prev_ts ):
                        if( self.ts_flag ):
                            if( lts_flag ):
                                del self.secondary[:]
                                self.ts_flag = False
                            else:
                                del self.primary[:]
                                self.primary.append( [ sum( [[settType, contractType, tenorType, maturity], row.text.split(' ')], [] ) ] )
                                self.ts_flag = False
                        elif( not(self.ts_flag) and not(lts_flag) ):
                            self.primary.append( [ sum( [[settType, contractType, tenorType, maturity], row.text.split(' ')], [] ) ] )

                    elif( row_ts < self.prev_ts ):
                        for item in self.primary: self.l.add_value( 'deals', item )#; print item
                        del self.primary[:]
                        self.prev_ts = row_ts
                        if( not(lts_flag) ): self.primary.append( [ sum( [[settType, contractType, tenorType, maturity], row.text.split(' ')], [] ) ] )

                    if( lts_flag == True ):
                        if( latest_ts[1] == None ):
                            return True

                        elif ( row_ts == latest_ts[1] ):
                            self.cur.execute("""SELECT COUNT(ts) FROM %s WHERE tenor = '%s' AND ts > '%s' AND ts <= '%s' """\
                                        % (self.table, tenorType, latest_ts[1], latest_ts[0]) )
                            count = self.cur.fetchone()[0]
                            for item in self.secondary[:-count]: self.l.add_value( 'deals', item )#; print item
                            del self.secondary[:]
                            return True

                        else:
                            self.secondary.append( [ sum( [[settType, contractType, tenorType, maturity], row.text.split(' ')], [] ) ] )
                            self.prev_ts = row_ts

                except (TypeError):
                    self.prev_ts = row_ts
                    if( not(lts_flag) ): self.primary.append( [ sum( [[settType, contractType, tenorType, maturity], row.text.split(' ')], [] ) ] )

        return False


    def traverse_pages(self, num_pages, eDealsTable, y, m, d, settType, contractType, tenorType, maturity, lts, index=0):
        for i in range( num_pages ):
            pages = self.selenium.find_elements_by_xpath('//*[@id="grdDeal"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')
            #print pages[i+index].text
            pages[i+index].click()
            if( self.read_table_rows( eDealsTable(), y, m, d, settType, contractType, tenorType, maturity, lts ) ):
               return True

        return False


    def invisible_pages(self, link, eDealsTable, y, m, d, settType, contractType, tenorType, maturity, lts):
        link.click()
        trades_rows = self.selenium.find_elements_by_xpath('//*[@id="grdDeal"]/tbody//tr')[1:-2]
        if( self.read_table_rows( eDealsTable(), y, m, d, settType, contractType, tenorType, maturity, lts ) ):
            return

        inv_pages = self.selenium.find_elements_by_xpath('//*[@id="grdDeal"]/tbody//tr')[-1].find_elements_by_xpath('.//td//*')
        index = 0
        while( inv_pages[index].tag_name != 'span' ):
            index+=1

        #print inv_pages[index].text
        inv_pages = self.selenium.find_elements_by_xpath('//*[@id="grdDeal"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')

        if (inv_pages[-1].text == '...' ):
            if ( self.traverse_pages( len(inv_pages)-2, eDealsTable, y, m, d, settType, contractType, tenorType, maturity, lts,  index ) ):#skip 2 '...' links
                return
            link = self.selenium.find_elements_by_xpath('//*[@id="grdDeal"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')[-1]
            self.invisible_pages( link, eDealsTable, y, m, d, settType, contractType, tenorType, maturity, lts)
        else:
            try:
                if ( self.traverse_pages( len(inv_pages)-1, eDealsTable, y, m, d, settType, contractType, tenorType, maturity, lts, index) ):#skip '...' link
                    return
            except:#when index is out of range
                self.get_start_page()


    def get_start_page(self):
        pages = self.selenium.find_elements_by_xpath('//*[@id="grdDeal"]/tbody//tr')[-1].find_elements_by_xpath('.//td//*')
        if ( pages[0].text == '...' ):
            pages[0].click()
            self.get_start_page()
        else:
            pages[0].click()
            return


    def parse(self, response):

        deals = CALL_DEALS()
        self.l = ItemLoader( item = deals, response = response )

        self.l.add_value('username', self.username)
        self.l.add_value('database', self.database)
        self.l.add_value('table', self.table)

        self.selenium.get(response.url)

        today = dt.datetime.today()
        y, m, d = today.year, today.month, today.day

        xpathTextField = '//*[@id="lblISMT_IDNT"]'
        xpathMaturityField = '//*[@id="lblMaturity"]'

        eSettTypes = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlSTLM_INDC"]')
        eContractTypes = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlISMT_INDC"]')
        eTenorTypes = lambda: self.selenium.find_element_by_xpath('//*[@id="ddlISMT_IDNT"]')

        eGO = lambda: self.selenium.find_element_by_xpath('//*[@id="btnGo"]')

        eMaturity = lambda: self.selenium.find_element_by_xpath('//*[@id="lblMaturity"]')
        eTimestamp = lambda: self.selenium.find_element_by_xpath('//*[@id="lblTime"]')
        eDealsTable = lambda: self.selenium.find_elements_by_xpath('//*[@id="grdDeal"]/tbody//tr')[1:-2]

        settTypes = [ x.text for x in Select( eSettTypes() ).options[1:] ]

        maturityField = eMaturity().text

        for i1 in settTypes:

            Select( eSettTypes() ).select_by_visible_text( i1 )

            if ( i1 == settTypes[1] ):
                try:
                    WebDriverWait(self.selenium, 5).until_not( lambda x: x.find_element_by_xpath( xpathMaturityField ).text == maturityField )
                    maturityField = eMaturity().text
                except (TypeError, TimeoutException):
                    pass #CALL - continue to check other contract types

            contractTypes = [ x.text for x in Select( eContractTypes() ).options ]

            for i2 in contractTypes:

                Select( eContractTypes() ).select_by_visible_text( i2 )

                try:
                    WebDriverWait(self.selenium, 5).until_not( lambda x: x.find_element_by_xpath( xpathMaturityField ).text == maturityField )
                    maturityField = self.selenium.find_element_by_xpath( xpathMaturityField ).text
                except ( TypeError, NoSuchElementException ):
                    continue
                except ( TimeoutException ):
                    if ( maturityField == None ):
                        continue

                tenorTypes = [ x.text for x in Select( eTenorTypes() ).options ]

                for i3 in tenorTypes:

                    Select( eTenorTypes() ).select_by_visible_text( i3 )

                    eGO().click()

                    if ( i2 == contractTypes[1] or i2 == contractTypes[2] ):
                        s = i2 + " Money: " + ''.join(i3.split(' ')) + " (" + ''.join(i1.split(' ')) + ")"
                    elif ( i2 == contractTypes[0] ):
                        s = i2 + " Money (" + ''.join(i1.split(' ')) + ")"

                    try:
                        WebDriverWait(self.selenium, 5).until( EC.text_to_be_present_in_element( (By.XPATH, '//*[@id="lblISMT_IDNT"]' ), s ) )
                    except ( TimeoutException ):
                        continue

                    latest_ts = []
                    self.cur.execute("""SELECT MAX(ts) FROM %s WHERE tenor = '%s'""" % (self.table, i3) )
                    latest_ts.append( self.cur.fetchone()[0] )
                    if ( latest_ts[0] == None ):
                        latest_ts = [None, None]
                    elif ( latest_ts[0].date() < today.date() ):
                        latest_ts = [None, None]
                    else:
                        self.cur.execute("""SELECT MAX(ts) FROM %s WHERE tenor = '%s' AND ts < '%s'""" % (self.table, i3, latest_ts[0]) )
                        latest_ts.append( self.cur.fetchone()[0] )
                        if ( latest_ts[1] != None and latest_ts[1].date() < today.date() ): latest_ts[1] = None

                    #print i1, i2, i3, latest_ts
                    self.ts_flag, self.prev_ts, self.primary, self.secondary = False, 0, [], []

                    if( self.read_table_rows( eDealsTable(), y, m, d, i1, i2, i3, eMaturity().text, latest_ts ) ):
                        continue

                    #links to other pages
                    try:
                        pages = self.selenium.find_elements_by_xpath('//*[@id="grdDeal"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')
                        #print pages

                        if ( pages[-1].text == "..." ):
                            if ( self.traverse_pages( len(pages)-1, eDealsTable, y, m, d, settType=i1, contractType=i2, tenorType=i3, maturity=eMaturity().text, lts=latest_ts ) ):
                                self.get_start_page()
                                continue
                            link = self.selenium.find_elements_by_xpath('//*[@id="grdDeal"]/tbody//tr')[-1].find_elements_by_xpath('.//td//a')[-1]
                            self.invisible_pages( link, eDealsTable, y, m, d, settType=i1, contractType=i2, tenorType=i3, maturity=eMaturity().text, lts=latest_ts )
                            for item in self.primary: self.l.add_value( 'deals', item )
                            del self.primary[:]
                            self.get_start_page()
                        else:
                            if ( self.traverse_pages( len(pages), eDealsTable, y, m, d, settType=i1, contractType=i2, tenorType=i3, maturity=eMaturity().text, lts=latest_ts ) ):
                                self.get_start_page()
                                continue
                            for item in self.primary: self.l.add_value( 'deals', item )
                            del self.primary[:]
                            self.get_start_page()
                    except (IndexError) :
                        for item in self.primary: self.l.add_value( 'deals', item )
                        del self.primary[:]

        self.l.load_item()
        self.__del__()
        return deals
