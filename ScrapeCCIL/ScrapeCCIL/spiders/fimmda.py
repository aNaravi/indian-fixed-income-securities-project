from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy.contrib.loader import ItemLoader
from scrapy.http import Request
from ScrapeCCIL.items import FIMMDA

import psycopg2
import datetime as dt
import re

class fimmda(Spider):

    name = 'fimmda'
    start_urls = [ 'https://www.fimmdareporting.co.in/CB_SEC_MEM_MARK_WATC_VIEW.aspx' ]
    fimmda = FIMMDA()
    l = ItemLoader( item = fimmda )

    def __init__(self, username, database):

        self.con = psycopg2.connect(database = database, user = username)
        self.cur = self.con.cursor()

        self.cur.execute('''CREATE TABLE IF NOT EXISTS fimmda_cd_cp
        (
            ts timestamp without time zone NOT NULL,
            isin character varying NOT NULL,
            issuer character varying NOT NULL,
            type character varying NOT NULL,
            maturity_date date NOT NULL,
            resi_days integer NOT NULL,
            sett_type character varying NOT NULL,
            trades integer NOT NULL,
            total_traded_amount real NOT NULL,
            last_trade_price real NOT NULL,
            last_trade_yield real NOT NULL,
            open real NOT NULL,
            high real NOT NULL,
            low real NOT NULL,
            weighted_avg_price real NOT NULL,
            weighted_avg_yield real NOT NULL
        )''')

        self.con.commit()

        self.l.add_value('username', username)
        self.l.add_value('database', database)

        self.RE = re.compile('\\r\\n.*')

        Spider.__init__(self)


    def __del__(self):
        self.cur.close()
        self.con.close()


    def make_requests_from_url(self, url):

        return {
            'https://www.fimmdareporting.co.in/CD_SEC_MEM_MARK_WATC_VIEW.aspx':Request( url, callback=self.parse_cd ),
            'https://www.fimmdareporting.co.in/CP_SEC_MEM_MARK_WATC_VIEW.aspx':Request( url, callback=self.parse_cp ),
            'https://www.fimmdareporting.co.in/CB_SEC_MEM_MARK_WATC_VIEW.aspx':Request( url, callback=self.parse_cb )
        }[url]


    def parse_cd(self, response):
        sel = Selector(response)

        l = ItemLoader( item = self.fimmda, response = response )
        ts = dt.datetime.now()

        table = sel.xpath('//*[@id="ctl00_SuperMainContent_grvCDSecondaryMarketWatch"]//tr')[1:-1]

        for row in table:
            r = [ x for x in row.xpath('.//text()').extract() if not(self.RE.match(x)) ]
            self.l.add_value( 'cd', [sum( [[ts, r[0], ' '.join(r[1:-12]), 'CD'], r[-12:]], [] )] )

        self.l.load_item()
        self.__del__()
        return self.fimmda


    def parse_cp(self, response):
        sel = Selector(response)

        l = ItemLoader( item = self.fimmda, response = response )
        ts = dt.datetime.now()

        table = sel.xpath('//*[@id="ctl00_SuperMainContent_gdvCPSecondaryMarketWatch"]//tr')[1:-1]

        for row in table:
            r = [ x for x in row.xpath('.//text()').extract() if not(self.RE.match(x)) ]
            self.l.add_value( 'cp', [sum( [[ts, r[0], ' '.join(r[1:-12]), 'CP'], r[-12:]], [] )] )

        yield self.make_requests_from_url( url='https://www.fimmdareporting.co.in/CD_SEC_MEM_MARK_WATC_VIEW.aspx' )


    def parse_cb(self, response):
        sel = Selector(response)

        l = ItemLoader( item = self.fimmda, response = response )
        ts = dt.datetime.now()

        table = sel.xpath('//*[@id="ctl00_SuperMainContent_gdvViewMarktWatch"]//tr')[1:-1]

        for row in table:
            r = [ x for x in row.xpath('.//text()').extract() if not(self.RE.match(x)) ]
            self.l.add_value( 'cb', [sum( [[ts, r[0], ' '.join(r[1:-13])], r[-13:]], [] )] )

        yield self.make_requests_from_url( url='https://www.fimmdareporting.co.in/CP_SEC_MEM_MARK_WATC_VIEW.aspx' )
