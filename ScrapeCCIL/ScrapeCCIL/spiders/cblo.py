from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from scrapy.contrib.loader import ItemLoader
from scrapy.http import Request
from ScrapeCCIL.items import CBLO

import psycopg2
import datetime as dt
import re

class cblo(CrawlSpider):

    name = 'cblo'
    start_urls = ['https://www.ccilindia.com/_Layouts/MarketToday/CBLOForexData.aspx?PageType=MByYield',
                  'https://www.ccilindia.com/_Layouts/MarketToday/CBLOForexData.aspx?PageType=ActiveCBLO']

    rules = (
        Rule( SgmlLinkExtractor( restrict_xpaths=('//*[@id="tblYield"]//a') ), callback="parse_quotes", follow= True ),

    )


    def __init__(self, username, database):

        self.username, self.database = username, database

        con = psycopg2.connect(database = database, user = username)
        cur = con.cursor()

        cur.execute('''CREATE TABLE IF NOT EXISTS cblo_summary
        (
            sr_no integer NOT NULL,
            ts timestamp without time zone NOT NULL,
            cblo_id character varying NOT NULL,
            sett_type character varying NOT NULL,
            open real NOT NULL,
            high real NOT NULL,
            low real NOT NULL,
            weighted_avg_rate real NOT NULL,
            last_trade_yield real NOT NULL,
            last_trade_amount real NOT NULL,
            last_trade_timestamp timestamp without time zone NOT NULL,
            total_traded_amount real NOT NULL,
            trades integer NOT NULL
        )''')

        cur.execute('''CREATE TABLE IF NOT EXISTS cblo_quotes
        (
            sr_no integer NOT NULL,
            cblo_id character varying NOT NULL,
            ts timestamp without time zone NOT NULL,
            lending_no real,
            lending_amt real,
            lending_rate real,
            borrowing_rate real,
            borrowing_amt real,
            borrowing_no integer
        )''')

        con.commit()

        cur.execute('''select max(sr_no) from cblo_quotes where ts >= %s''', [dt.datetime.date(dt.datetime.today())] )
        try:
            self.sr_no = cur.fetchone()[0] + 1
        except (TypeError):
            self.sr_no = 1

        self.RE = re.compile('[^\\r\\n].*')
        self.cblo_s = CBLO()

        CrawlSpider.__init__(self)

        cur.close()
        con.close()


    def parse_start_url(self, response):

        sel = Selector(response)
        l = ItemLoader( item = self.cblo_s )

        l.add_value('username', self.username)
        l.add_value('database', self.database)

        if( sel.xpath('//*[@id="divMarketByYield"]/@style').extract()[0] == u'display:block;' ):
            summaryTable = sel.xpath('//*[@id="tblYield"]//tr')[2:]
            ts = sel.xpath('//*[@id="lblYieldMarket"]//text()').extract()[0]

            for row in summaryTable:
                l.add_value( 'summary1', [ sum( [ [self.sr_no, ts], filter( self.RE.match, row.xpath('.//text()').extract() ) ], [] ) ] )

        elif( sel.xpath('//*[@id="divActiveCBLO"]/@style').extract()[0] == u'display:block;' ):

            summaryTable = sel.xpath('//*[@id="tblActiveCBLO"]//tr')[5:-1]
            ts = sel.xpath('//*[@id="lblActiveCBLO"]//text()').extract()[0]

            for row in summaryTable:
                l.add_value( 'summary2', [ sum( [ [self.sr_no, ts], filter( self.RE.match, row.xpath('.//text()').extract() ) ], [] ) ] )

        l.load_item()
        return self.cblo_s


    def parse_quotes(self, response):

        sel = Selector(response)
        cblo = CBLO()
        l = ItemLoader( item = cblo )

        l.add_value('username', self.username)
        l.add_value('database', self.database)

        bidTable = sel.xpath('//*[@id="tblBuy"]//tr')[2:]
        offerTable = sel.xpath('//*[@id="tblOffer"]//tr')[2:]
        ts = sel.xpath('//*[@id="lblYieldMarket"]//text()').extract()[0]
        cblo_id = sel.xpath('//*[@id="tblYield"]//tr[@bgcolor="Yellow"]').xpath('.//a//text()').extract()[0]

        for b,o in zip(bidTable, offerTable):
            l.add_value( 'quotes', [ sum([ [self.sr_no, cblo_id, ts], filter(self.RE.match, b.xpath('.//text()').extract()), filter(self.RE.match, o.xpath('.//text()').extract()) ], [] ) ] )

        l.load_item()
        return cblo



