# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field
from scrapy.contrib.loader import processor

class OM_MKT_BY_PRICE(Item):

    username = Field()
    database = Field()
    quotes = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-8:] != [u'-']*8 else None ) )
    summary = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-6:] != [u'-']*6 else None ) )


class OM_TRADES(Item):

    username = Field()
    database = Field()
    table = Field()
    trades = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-4:] != [u'-']*4 else None ) )


class OM_REPORTED_DEALS(Item):

    username = Field()
    database = Field()
    table = Field()
    deals = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-4:] != [u'-']*4 else None ) )


class CALL_MKT_BY_RATE(Item):

    username = Field()
    database = Field()
    quotes = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-4:] != [u'-']*4 else None ) )
    summary = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-8:] != [u'-']*8 else None ) )


class CALL_DEALS(Item):

    username = Field()
    database = Field()
    table = Field()
    deals = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-3:] != [u'-']*3 else None ) )


class CALL_REPORTED_DEALS(Item):

    username = Field()
    database = Field()
    table = Field()
    deals = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-3:] != [u'-']*3 else None ) )


class CROMS_MKT_BY_RATE(Item):

    username = Field()
    database = Field()
    summary = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-8:] != [u'-']*8 else None ) )
    quotes = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-6:] != [u'-']*6 else None ) )


class CROMS_DEALS(Item):

    username = Field()
    database = Field()
    table = Field()
    deals = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-4:] != [u'-']*4 else None ) )


class CROMS_REPORTED_DEALS(Item):

    username = Field()
    database = Field()
    table = Field()
    deals = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-4:] != [u'-']*4 else None ) )


class FIMMDA(Item):

    username = Field()
    database = Field()
    cd = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-12:] != [u'-']*12 else None ) )
    cp = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-12:] != [u'-']*12 else None ) )
    cb = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-13:] != [u'-']*13 else None ) )


class DERIVATIVES(Item):

    username = Field()
    database = Field()
    irs_ff = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-7:] != [u' ']*7 else None ) )
    fra = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-7:] != [u' ']*7 else None ) )


class FOREX(Item):

    username = Field()
    database = Field()
    summary = Field( input_processor = processor.MapCompose(lambda x: [x] if x[-4:] != ['']*4 else None) )
    quotes = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-6:] != [u'-']*6 else None ) )


class CBLO(Item):

    username = Field()
    database = Field()
    summary1 = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-10:] != [u'-']*10 else None ) )
    summary2 = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-13:] != [u'-']*13 else None ) )
    quotes = Field( input_processor = processor.MapCompose( lambda x: [x] if x[-6:] != [u'-']*6 else None ) )
