# Scrapy settings for ScrapeCCIL project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'ScrapeCCIL'

SPIDER_MODULES = ['ScrapeCCIL.spiders']
NEWSPIDER_MODULE = 'ScrapeCCIL.spiders'
ITEM_PIPELINES = {
    'ScrapeCCIL.pipelines.ccilPipeline': 100
}
#AUTOTHROTTLE_ENABLED = True
#AUTOTHROTTLE_DEBUG = True
#AUTOTHROTTLE_START_DELAY = 5
#CONCURRENT_REQUESTS_PER_DOMAIN = 2
#DOWNLOAD_DELAY = 0.25    

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'ScrapeCCIL (+http://www.yourdomain.com)'
