import sys
import os
import shutil
import datetime as dt
import zipfile
import xlrd
import csv
import re
import psycopg2
import numpy as np


''' IMPLEMENTATION NOTE:
    To handle the various formats the data is presented in, a separate template(class) is created for each format.
    When initialzing each class, keep the zipfiles of that format in a seperate folder, and pass that folder's location to
    as a parameter.
'''

class format1:

    def __init__(self, dirName, cur, con):

        self.con = con
        self.cur = cur

        self._dirName = dirName #contains location of zipfiles
        os.chdir(self._dirName)

        self._zfiles = [ f for f in os.listdir(self._dirName) if os.path.isfile(os.path.join(self._dirName,f)) ]
        self._benchmark_rates = set()
        self._cblo = set()
        self._gov_sec = set()
        #self._trade_data = set()
        self._trade_summary = set()
        self._forex_vol = set()
        self._term_money_outstanding = set()
        self._forex_intercategory = set()
        self._repo_intercategory = set()
        self._gsec_market_share = { 'NDS-OM/OTC' : set(), 'Constituent Deals' : set() }
        self._otc_outright_forex_intercategory = set()
        self._otc_gsec_intercategory = set()
        self._otc_tbill_intercategory = set()
        self._otc_trade_details = set()
        self._ndsom_trade_details = set()
        self._when_issued_trade_details = set()
        self._irs_transactions = set()
        self._sdl_index = set()
        self._bond_index = set()
        self._tenor_index = set()
        self._tbill_index = set()
        self._all_sovr_bond_index = set()
        self._spot_rate = set()
        self._state_gsec_spread = set()
        self._ccbid_ccbor = set()
        self._mibid_mibor = set()
        self._cds_transaction_summary = set()


    def read(self):
        self._zfiles.sort()

        for z in self._zfiles:
            unzip(z, z[:-4])
            self.read_files(z[:-4])
            shutil.rmtree(z[:-4])


    def read_files(self, dirName):

        os.chdir(dirName)
        mypath = os.getcwd()
        files = [ f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f)) ]

        date = dt.date(int(dirName[5:9]), int(dirName[10:12]), int(dirName[13:]))
        print date

        CCILs = {'CCIL01.xls' : lambda x: self.read_CCIL01(x), 'CCIL02.xls' : lambda x: self.read_CCIL02(x),
                 'CCIL0809.xls' : lambda x: self.read_CCIL0809(x), 'CCIL10.xls' : lambda x: self.read_CCIL10(x),
                 'CCIL1213.xls' : lambda x: self.read_CCIL1213(x), 'CCIL14.xls' : lambda x: self.read_CCIL14(x),
                 'CCIL15.xls' : lambda x: self.read_CCIL15(x), 'CCIL16.xls' : lambda x: self.read_CCIL16(x),
                 'CCIL1718.xls' : lambda x: self.read_CCIL1718(x), 'CCIL19.xls' : lambda x: self.read_CCIL19(x),
                 'CCIL20.xls' : lambda x: self.read_CCIL20(x), 'CCIL21.xls' : lambda x: self.read_CCIL21(x),
                 'CCIL22.xls' : lambda x: self.read_CCIL22(x), 'CCIL23.xls' : lambda x: self.read_CCIL23(x),
                 'CCIL24.xls' : lambda x: self.read_CCIL24(x), 'CCIL26.xls' : lambda x: self.read_CCIL26(x),
                 'CCIL27.xls' : lambda x: self.read_CCIL27(x), 'CCIL29.xls' : lambda x: self.read_CCIL29(x),
                 'CCIL30.xls' : lambda x: self.read_CCIL30(x), 'CCIL31.xls' : lambda x: (x),
                 'CCIL32.xls' : lambda x: self.read_CCIL32(x), 'CCIL33.xls' : lambda x: self.read_CCIL33(x),
                 'CCIL03.xls' : lambda x: self.read_CCIL03(x), 'CCIL34.xls' : lambda x: self.read_CCIL34(x),
                 'CCIL0407.xls' : lambda x: x, 'CCIL25.xls' : lambda x: x}

        p = re.compile('CCIL.+')
        for f in files:
            if( p.match(f) ):
                try:
                    CCILs[ p.match(f).group() ](date)
                except (KeyError):
                    print date, p.match(f).group()
                    continue


        os.chdir(self._dirName)


    def read_CCIL01(self, date):

        book = xlrd.open_workbook('CCIL01.xls')
        sheet = book.sheet_by_index(0)

        self._benchmark_rates.add( tuple( sum( [ [date], sheet.col_values(1, 1, sheet.nrows) ], []  ) ) )


    def read_CCIL02(self, date):

        book = xlrd.open_workbook('CCIL02.xls')
        sheet = book.sheet_by_index(2)

        self._forex_vol.add( tuple( sum( [ [date], sheet.row_values(8, 1, 4),
                                              sheet.row_values(9, 1, 4),
                                              sheet.row_values(10, 1, 4),
                                              sheet.row_values(11, 1, 4) ], [] ) ) )

        sheet = book.sheet_by_index(1)

        for i in range(1, sheet.nrows-1):
            row = sheet.row_values(i)
            try:
                row = [ dt.datetime(*(xlrd.xldate_as_tuple(x, book.datemode))) if row.index(x) == 1 else None if x == '' else x  for x in row ]
            except (ValueError):
                months_map = { 'JAN': 1, 'FEB':2, 'MAR':3, 'APR':4, 'MAY':5, 'JUN':6, 'JUL':7, 'AUG':8, 'SEP':9, 'OCT':10, 'NOV':11, 'DEC':12 }
                get_d = lambda x: [ int(x[0]), months_map[x[1]], int(x[2]) ]
                row = sheet.row_values(i,1)
                try:
                    row = [ dt.datetime( *( get_d( x.split('-') ) ) ) if row.index(x) == 1 else None if x == '' else x  for x in row ]
                except:
                    continue
                if( len(row) == 10 ) :
                    self._trade_summary.add( tuple( sum( [ [date], row ], [] ) ) )
                    #self._trade_data.add( tuple( sum( [ [date], row ], [] ) ) )
                else:
                    print date, row
            else:
                if( len(row) == 10 ) :
                    self._trade_summary.add( tuple( sum( [ [date], row ], [] ) ) )
                    #self._trade_data.add( tuple( sum( [ [date], row ], [] ) ) )
                else:
                    print date, row


    def read_CCIL03(self, date):

        book = xlrd.open_workbook('CCIL03.xls')
        sheet = book.sheet_by_index(0)

        for i in range(3, sheet.nrows-1):
            row = filter( lambda x: x != '-', sum( [ sheet.row_values(i,0,6), sheet.row_values(i,7) ], [] ) )
            try:
                c1, c2, c3, c4 = row[0], row[1], row[2], row[3][2]
                c5 = dt.timedelta( days = row[2] ) + date
                c6, c7, c8, c9, c10, c11, c12 = row[4:]
            except:
                return

            self._cblo.add( tuple( [date, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12 ] ) )



    '''def read_CCIL0407(self, date):

        book = xlrd.open_workbook('CCIL0407.xls')
        sheet = book.sheet_by_index(0)

        for i in range(2, sheet.nrows-1):
            row = sheet.row_values(i)
            try:
                row = filter( lambda x: x!='', [ dt.datetime(*(xlrd.xldate_as_tuple(x, book.datemode))) if row.index(x) == 1 else x for x in row ] )
            except (ValueError):
                months_map = { 'JAN': 1, 'FEB':2, 'MAR':3, 'APR':4, 'MAY':5, 'JUN':6, 'JUL':7, 'AUG':8, 'SEP':9, 'OCT':10, 'NOV':11, 'DEC':12 }
                get_d = lambda x: [ int(x[0]), months_map[x[1]], int(x[2]) ]
                row = sheet.row_values(i,1)
                row = filter( lambda x: x!='', [ dt.datetime( *( get_d( x.split('-') ) ) ) if row.index(x) == 1 else x for x in row ] )
            else:
                if( len(row) > 0 ) : self._trade_summary.add( tuple( sum( [ [date], row ], [] ) ) )'''


    def read_CCIL0809(self, date):

        book = xlrd.open_workbook('CCIL0809.xls')
        sheet = book.sheet_by_index(0)

        tables = { 'Buy Side' : range(4,10), 'Sell Side' : range(14,20) }

        for key in tables.iterkeys():
            for row in tables[key]:
                self._otc_outright_forex_intercategory.add( tuple( sum( [ [date, key], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL10(self, date):

        book = xlrd.open_workbook('CCIL10.xls')
        sheet = book.sheet_by_index(1)

        for i in range( 1, sheet.nrows ):
            row = sheet.row_values(i,0,7)
            row = [ dt.datetime(*(xlrd.xldate_as_tuple(x, book.datemode))) if row.index(x) == 0 or row.index(x) == 1 else None if x == '' else x for x in row ]
            self._term_money_outstanding.add( tuple( sum( [ [date], row ], [] ) ) )


    def read_CCIL1213(self, date):

        book = xlrd.open_workbook('CCIL1213.xls')
        sheet = book.sheet_by_index(0)

        markets = { 'Cash' : range(6,11),
                    'Tom' : range(17,22),
                    'Spot' : range(28,33),
                    'Forward' : range(39,44) }

        for mkt in markets.iterkeys():
            for row in markets[mkt]:
                self._forex_intercategory.add( tuple( sum( [ [date, mkt], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL14(self, date):

        book = xlrd.open_workbook('CCIL14.xls')
        sheet = book.sheet_by_index(0)

        tables = {'Reverse Repo'  : [ 'Lender', range(5,11) ],
                  'Repo'          : [ 'Borrower', range(15,21) ]}

        for key in tables.iterkeys():
            for row in tables[key][1]:
                self._repo_intercategory.add( tuple( sum( [ [ date, key, tables[key][0] ], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL15(self, date):

        book = xlrd.open_workbook('CCIL15.xls')
        sheet = book.sheet_by_index(0)

        for i in range(5,11):
            row = sheet.row_values(i)
            if( date < dt.date(2013, 9, 26) ):
                row.extend( [None, None] )
            self._gsec_market_share['NDS-OM/OTC'].add( tuple( sum( [ [ date ], [ x if type(x)!=str else None for x in row  ] ], [] ) ) )

        for i in range(17,19):
            self._gsec_market_share['Constituent Deals'].add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(i) if type(x)!=str ] ], [] ) ) )


    def read_CCIL16(self, date):

        book = xlrd.open_workbook('CCIL16.xls')
        sheet = book.sheet_by_index(0)

        self._sdl_index.add( tuple( sum( [ [ date ], [ x if type(x)!=str else None for x in sheet.row_values(2,1,6)] ], [] ) ) )


    def read_CCIL1718(self, date):

        book = xlrd.open_workbook('CCIL1718.xls')
        sheet = book.sheet_by_index(0)

        for row in range(4, sheet.nrows):
            values = sheet.row_values(row)
            self._state_gsec_spread.add( tuple( sum( [ [date], \
            filter(None, [ dt.datetime(*(xlrd.xldate_as_tuple(x, book.datemode))) if values.index(x) == 1 else x for x in values ]) ], [] ) ) )


    def read_CCIL19(self, date):

        book = xlrd.open_workbook('CCIL19.xls')
        sheet = book.sheet_by_index(0)

        self._bond_index.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(2,1) if type(x)!=str ] ], [] ) ) )


    def read_CCIL20(self, date):

        book = xlrd.open_workbook('CCIL20.xls')
        sheet = book.sheet_by_index(0)

        for row in [2,3]:
            self._tenor_index.add( tuple( sum( [ [date], [ x if type(x)!=str else None for x in sheet.row_values(row,0,6) ] ], [] ) ) )


    def read_CCIL21(self, date):

        book = xlrd.open_workbook('CCIL21.xls')
        sheet = book.sheet_by_index(0)

        self._tbill_index.add( tuple( sum( [ [ date ], [ x if type(x)!=str else None for x in sheet.row_values(2,1,5) ] ], [] ) ) )


    def read_CCIL22(self, date):

        book = xlrd.open_workbook('CCIL22.xls')
        sheet = book.sheet_by_index(0)

        self._all_sovr_bond_index.add(tuple( sum( [ [ date ], [ x for x in sheet.row_values(2,1) if type(x)!=str ] ], [] ) )  )


    def read_CCIL23(self, date):

        book = xlrd.open_workbook('CCIL23.xls')
        sheet = book.sheet_by_index(0)

        tables = { 'Buy Side' : range(4,10), 'Sell Side' : range(14,20) }

        for key in tables.iterkeys():
            for row in tables[key]:
                self._otc_gsec_intercategory.add( tuple( sum( [ [date, key], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL24(self, date):

        book = xlrd.open_workbook('CCIL24.xls')
        sheet = book.sheet_by_index(0)

        tables = { 'Buy Side' : range(4,10), 'Sell Side' : range(14,20) }

        for key in tables.iterkeys():
            for row in tables[key]:
                self._otc_tbill_intercategory.add( tuple( sum( [ [date, key], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL26(self, date):

        book = xlrd.open_workbook('CCIL26.xls')
        sheet = book.sheet_by_index(0)

        for row in range(2, sheet.nrows):
            self._otc_trade_details.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL27(self, date):

        book = xlrd.open_workbook('CCIL27.xls')
        sheet = book.sheet_by_index(0)

        for row in range(2, sheet.nrows):
            self._ndsom_trade_details.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL29(self, date):

        book = xlrd.open_workbook('CCIL29.xls')
        sheet = book.sheet_by_index(0)

        for row in range(2, sheet.nrows-1):
            self._when_issued_trade_details.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL30(self, date):

        try:
            book = xlrd.open_workbook('CCIL30.xls')
        except:
            return
        sheet = book.sheet_by_index(0)

        for row in [1,2]:
            self._ccbid_ccbor.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(row,1) if type(x)!=str ] ], [] ) ) )


    def read_CCIL31(self, date):

        book = xlrd.open_workbook('CCIL31.xls')
        sheet = book.sheet_by_index(0)

        for row in [1,2]:
            self._mibid_mibor.add( tuple( sum( [ [ date ], [ x if type(x)!=str else None for x in sheet.row_values(row,1,6) ] ], [] ) ) )


    def read_CCIL32(self, date):

        book = xlrd.open_workbook('CCIL32.xls')
        sheet = book.sheet_by_index(0)

        tables = { 'MIBOR' : range(3,15), 'MIFOR' : range(19,26) }

        for benchmark in tables.iterkeys():
            for i in tables[benchmark]:
                row = [ None if x == ' ' else x if type(x)!=str else None for x in sheet.row_values(i,0,8) ]
                self._irs_transactions.add( tuple( sum( [ [ date, benchmark ], row ], [] ) ) )


    def read_CCIL33(self, date):

        book = xlrd.open_workbook('CCIL33.xls')
        sheet = book.sheet_by_index(0)

        self._spot_rate.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(2,1) if type(x)!=str ] ], [] ) ) )


    def read_CCIL34(self, date):

        book = xlrd.open_workbook('CCIL34.xls')
        sheet = book.sheet_by_index(0)

        for i in range(4, sheet.nrows-1):
            self._cds_transaction_summary.add( tuple( sum( [ [date], filter(None, sheet.row_values(i)) ], [] ) ) )


    def get_queries(self):

        column_str = dict()
        insert_str = dict()
        query = dict()

        column_str['cblo'] = 'transaction_date, instrument, market_type, days_to_maturity, settlement_type, maturity_date, \
                                number_of_trades, volume, open_price, high_price, low_price, close_price, weighted_avg'
        insert_str['cblo'] = ('%s, '*13)[:-2]
        query['cblo'] = 'INSERT INTO cblo (%s) VALUES (%s)' % (column_str['cblo'], insert_str['cblo'])

        '''column_str['trade_data'] = 'transaction_date, sec_description, maturity_date, number_of_trades, total_volume, \
                                    high_price, low_price, last_traded_price, weighted_avg_price, weighted_avg_yield'
        insert_str['trade_data'] = ('%s, '*11)[:-2]
        query['trade_data'] = 'INSERT INTO trade_data (%s) VALUES (%s)' % (column_str['trade_data'], insert_str['trade_data'])'''

        column_str['forex_vol'] = 'summary_date, cash_no_of_trades, cash_usd, cash_inr, tom_no_of_trades, tom_usd, tom_inr, \
                                spot_no_of_trades, spot_usd, spot_inr, forward_no_of_trades, forward_usd, forward_inr'
        insert_str['forex_vol'] = ('%s, '*13)[:-2]
        query['forex_vol'] = 'INSERT INTO forex_volumes (%s) VALUES (%s)' % (column_str['forex_vol'], insert_str['forex_vol'])

        column_str['benchmark_rates'] = 'dates, '
        lst = np.arange(0.5, 30.5, 0.5)
        for el in lst:
            column_str['benchmark_rates'] += '"' + str(el) + '", '

        insert_str['benchmark_rates'] = ('%s, '*61)[:-2]
        query['benchmark_rates'] = 'INSERT INTO benchmark_rates (%s) VALUES (%s)' % \
            (column_str['benchmark_rates'][:-2], insert_str['benchmark_rates'])

        column_str['trade_summary'] = 'summary_date, description, maturity, trades, volume, high, low, last_traded_price, last_traded_ytm, \
                                        weighted_avg_price, weighted_avg_yield'
        insert_str['trade_summary'] = ('%s, '*11)[:-2]
        query['trade_summary'] = 'INSERT INTO trade_summary (%s) VALUES (%s)' % (column_str['trade_summary'], insert_str['trade_summary'])

        column_str['term_money'] = 'summary_date, settlement_date, maturity_date, original_tenor, trades, amount_cr, residual_days, rate'
        insert_str['term_money'] = ('%s, '*8)[:-2]
        query['term_money'] = 'INSERT INTO term_money_stats (%s) VALUES (%s)' % (column_str['term_money'], insert_str['term_money'])

        column_str['forex_intercategory'] = 'summary_date, market_type, category, foreign_banks, public_sector_banks, private_sector_banks, \
                                            cooperative_banks, financial_institutions, total, percentage_of_market_share'
        insert_str['forex_intercategory'] = ('%s, '*10)[:-2]
        query['forex_intercategory'] = 'INSERT INTO forex_intercategory (%s) VALUES (%s)' % (column_str['forex_intercategory'], insert_str['forex_intercategory'])

        column_str['repo_intercategory'] = 'summary_date, market_type, dealer, category, foreign_banks, public_sector_banks, private_sector_banks, \
                                            mutual_funds, others, primary_dealers, total, percentage_of_market_share'
        insert_str['repo_intercategory'] = ('%s, '*12)[:-2]
        query['repo_intercategory'] = 'INSERT INTO repo_intercategory (%s) VALUES (%s)' % (column_str['repo_intercategory'], insert_str['repo_intercategory'])

        column_str['gsec_otc_ndsom'] = 'summary_date, category, outright_buy, outright_sell, gsec_buy, gsec_sell, tbill_buy, tbill_sell, sdl_buy, sdl_sell'
        insert_str['gsec_otc_ndsom'] = ('%s, '*10)[:-2]
        query['gsec_otc_ndsom'] = 'INSERT INTO gsec_otc_ndsom (%s) VALUES (%s)' % (column_str['gsec_otc_ndsom'], insert_str['gsec_otc_ndsom'])

        column_str['gsec_constituent_deals'] = 'summary_date, category, trades, volume, percentage_total_outright_volume'
        insert_str['gsec_constituent_deals'] =  ('%s, '*5)[:-2]
        query['gsec_constituent_deals'] = 'INSERT INTO gsec_constituent_deals (%s) VALUES (%s)' % (column_str['gsec_constituent_deals'], insert_str['gsec_constituent_deals'])

        column_str['otc_outright_forex_intercategory'] = 'summary_date, dealer, category, foreign_banks, public_sector_banks, \
                                        private_sector_banks, mutual_funds, others, primary_dealers, total, percentage_of_market_share'
        insert_str['otc_outright_forex_intercategory'] = ('%s, '*11)[:-2]
        query['otc_outright_forex_intercategory'] = 'INSERT INTO otc_outright_forex_intercategory (%s) VALUES (%s)' % \
                                                    (column_str['otc_outright_forex_intercategory'], insert_str['otc_outright_forex_intercategory'])

        column_str['otc_gsec_intercategory'] = 'summary_date, dealer, category, foreign_banks, public_sector_banks, \
                                        private_sector_banks, mutual_funds, others, primary_dealers, total, percentage_of_market_share'
        insert_str['otc_gsec_intercategory'] = ('%s, '*11)[:-2]
        query['otc_gsec_intercategory'] = 'INSERT INTO otc_gsec_intercategory (%s) VALUES (%s)' % (column_str['otc_gsec_intercategory'], insert_str['otc_gsec_intercategory'])

        column_str['otc_tbill_intercategory'] = 'summary_date, dealer, category, foreign_banks, public_sector_banks, \
                                        private_sector_banks, mutual_funds, others, primary_dealers, total, percentage_of_market_share'
        insert_str['otc_tbill_intercategory'] = ('%s, '*11)[:-2]
        query['otc_tbill_intercategory'] = 'INSERT INTO otc_tbill_intercategory (%s) VALUES (%s)' % (column_str['otc_tbill_intercategory'], insert_str['otc_tbill_intercategory'])

        column_str['otc_trade_details'] =  'summary_date, description, trades, volume, open, high, low, close, weighted_avg_price, weighted_avg_yield'
        insert_str['otc_trade_details'] = ('%s, '*10)[:-2]
        query['otc_trade_details'] = 'INSERT INTO otc_trade_details (%s) VALUES (%s)' % (column_str['otc_trade_details'], insert_str['otc_trade_details'])

        column_str['ndsom_trade_details'] = 'summary_date, description, trades, volume, open, high, low, close, weighted_avg_price, weighted_avg_yield'
        insert_str['ndsom_trade_details'] = ('%s, '*10)[:-2]
        query['ndsom_trade_details'] = 'INSERT INTO ndsom_trade_details (%s) VALUES (%s)' % (column_str['ndsom_trade_details'], insert_str['ndsom_trade_details'])

        column_str['when_issued_trade_details'] = 'summary_date, description, maturity_date, trades, volume, high, low, weighted_avg_price, weighted_avg_yield'
        insert_str['when_issued_trade_details'] = ('%s, '*9)[:-2]
        query['when_issued_trade_details'] = 'INSERT INTO when_issued_trade_details (%s) VALUES (%s)' % (column_str['when_issued_trade_details'], insert_str['when_issued_trade_details'])

        column_str['irs_transactions'] = 'summary_date, benchmark, tenors, trades, volume, high, low, close, last_reported_rate, weighted_avg_rate'
        insert_str['irs_transactions'] = ('%s, '*10)[:-2]
        query['irs_transactions'] = 'INSERT INTO irs_transactions (%s) VALUES (%s)' % (column_str['irs_transactions'], insert_str['irs_transactions'])

        column_str['sdl_index'] = 'summary_date, ccil_sdl_pri, ccil_sdl_tri, coupon, yield, duration'
        insert_str['sdl_index'] = ('%s, '*6)[:-2]
        query['sdl_index'] = 'INSERT INTO sdl_index (%s) VALUES (%s)' % (column_str['sdl_index'], insert_str['sdl_index'])

        column_str['bond_index'] = 'summary_date, ccil_broad_tri, ccil_broad_pri, ccil_liquid_01, ccil_liquid_02'
        insert_str['bond_index'] = ('%s, '*5)[:-2]
        query['bond_index'] = 'INSERT INTO bond_index (%s) VALUES (%s)' % (column_str['bond_index'], insert_str['bond_index'])

        column_str['tenor_index'] = 'summary_date, type, upto_5, years_5_10, years_10_15, years_15_20, years_20_30'
        insert_str['tenor_index'] = ('%s, '*7)[:-2]
        query['tenor_index'] = 'INSERT INTO tenor_index (%s) VALUES (%s)' % (column_str['tenor_index'], insert_str['tenor_index'])

        column_str['tbill_index'] = 'summary_date, ccil_liquidity_weight_index, duration_lwi, ccil_equal_weight_index, duration_ewi'
        insert_str['tbill_index'] = ('%s, '*5)[:-2]
        query['tbill_index'] = 'INSERT INTO tbill_index (%s) VALUES (%s)' % (column_str['tbill_index'], insert_str['tbill_index'])

        column_str['all_sovr_bond_index'] = 'summary_date, casbi_tri, casbi_pri, coupon, duration'
        insert_str['all_sovr_bond_index'] = ('%s, '*5)[:-2]
        query['all_sovr_bond_index'] = 'INSERT INTO all_sovreign_bond_index (%s) VALUES (%s)' % (column_str['all_sovr_bond_index'], insert_str['all_sovr_bond_index'])

        column_str['spot_rate'] = 'summary_date, rate, high, low, volatility'
        insert_str['spot_rate'] = ('%s, '*5)[:-2]
        query['spot_rate'] = 'INSERT INTO spot_rate (%s) VALUES (%s)' % (column_str['spot_rate'], insert_str['spot_rate'])

        column_str['state_gsec_spread'] = 'summary_date, description, maturity_date, trades, volume, avg_spread, close_price, close_ytm, weighted_avg_price, weighted_avg_yield'
        insert_str['state_gsec_spread'] = ('%s, '*10)[:-2]
        query['state_gsec_spread'] = 'INSERT INTO state_gsec_spread (%s) VALUES (%s)' % (column_str['state_gsec_spread'], insert_str['state_gsec_spread'])

        column_str['ccbid_ccbor'] = 'summary_date, time, ccbor, ccbor_st_dev, ccbid, ccbid_st_dev'
        insert_str['ccbid_ccbor'] = ('%s, '*6)[:-2]
        query['ccbid_ccbor'] = 'INSERT INTO ccbid_ccbor (%s) VALUES (%s)' % (column_str['ccbid_ccbor'], insert_str['ccbid_ccbor'])

        column_str['mibid_mibor'] = 'summary_date, time, mibor, mibor_volatility, mibid, mibid_volatility, waonr, dealta'
        insert_str['mibid_mibor'] = ('%s, '*8)[:-2]
        query['mibid_mibor'] = 'INSERT INTO mibid_mibor (%s) VALUES (%s)' % (column_str['mibid_mibor'], insert_str['mibid_mibor'])

        column_str['cds_summary'] = 'summary_date, tenor, trades, notional_amt, reference_entities, high_bps, low_bps'
        insert_str['cds_summary'] = ('%s, '*7)[:-2]
        query['cds_summary'] = 'INSERT INTO cds_transaction_summary (%s) VALUES (%s)' % (column_str['cds_summary'], insert_str['cds_summary'])

        return query


    def write_to_db(self):

        query = self.get_queries()

        self.cur.executemany(query['cblo'], self._cblo)

        #self.cur.executemany(query['trade_data'], self._trade_data)

        self.cur.executemany(query['forex_vol'], self._forex_vol)

        self.cur.executemany(query['benchmark_rates'], self._benchmark_rates)

        self.cur.executemany(query['trade_summary'], self._trade_summary)

        self.cur.executemany(query['term_money'], self._term_money_outstanding)

        self.cur.executemany(query['forex_intercategory'], self._forex_intercategory)

        self.cur.executemany(query['repo_intercategory'], self._repo_intercategory)

        self.cur.executemany(query['gsec_otc_ndsom'], self._gsec_market_share['NDS-OM/OTC'])

        self.cur.executemany(query['gsec_constituent_deals'], self._gsec_market_share['Constituent Deals'])

        self.cur.executemany(query['otc_outright_forex_intercategory'], self._otc_outright_forex_intercategory)

        self.cur.executemany(query['otc_gsec_intercategory'], self._otc_gsec_intercategory)

        self.cur.executemany(query['otc_tbill_intercategory'], self._otc_tbill_intercategory)

        self.cur.executemany(query['otc_trade_details'], self._otc_trade_details)

        self.cur.executemany(query['ndsom_trade_details'], self._ndsom_trade_details)

        self.cur.executemany(query['when_issued_trade_details'], self._when_issued_trade_details)

        self.cur.executemany(query['irs_transactions'], self._irs_transactions)

        self.cur.executemany(query['sdl_index'], self._sdl_index)

        self.cur.executemany(query['bond_index'], self._bond_index)

        self.cur.executemany(query['tenor_index'], self._tenor_index)

        self.cur.executemany(query['tbill_index'], self._tbill_index)

        self.cur.executemany(query['all_sovr_bond_index'], self._all_sovr_bond_index)

        self.cur.executemany(query['spot_rate'], self._spot_rate)

        self.cur.executemany(query['state_gsec_spread'], self._state_gsec_spread)

        self.cur.executemany(query['ccbid_ccbor'], self._ccbid_ccbor)

        #self.cur.executemany(query['mibid_mibor'], self._mibid_mibor)

        self.con.commit()


class format2:

    '''This class handles the data which is presented in the format 15-10-2013 - latest'''

    def __init__(self, dirName, cur, con):

        self.con = con
        self.cur = cur

        self._dirName = dirName #contains location of zipfiles
        os.chdir(self._dirName)

        self._zfiles = [ f for f in os.listdir(self._dirName) if os.path.isfile(os.path.join(self._dirName,f)) ]
        self._benchmark_rates = set()
        self._cblo = set()
        self._gov_sec = set()
        self._trade_data = set()
        self._trade_summary = set()
        self._forex_vol = set()
        self._term_money_outstanding = set()
        self._forex_intercategory = set()
        self._repo_intercategory = set()
        self._gsec_market_share = { 'NDS-OM/OTC' : set(), 'Constituent Deals' : set() }
        self._otc_outright_forex_intercategory = set()
        self._otc_gsec_intercategory = set()
        self._otc_tbill_intercategory = set()
        self._otc_trade_details = set()
        self._ndsom_trade_details = set()
        self._when_issued_trade_details = set()
        self._irs_transactions = set()
        self._sdl_index = set()
        self._bond_index = set()
        self._tenor_index = set()
        self._tbill_index = set()
        self._all_sovr_bond_index = set()
        self._spot_rate = set()
        self._state_gsec_spread = set()
        self._ccbid_ccbor = set()
        self._mibid_mibor = set()


    def read(self):
        self._zfiles.sort()

        for z in self._zfiles:
            unzip(z, z[:-4])
            self.read_files(z[:-4])
            shutil.rmtree(z[:-4])


    def read_files(self, dirName):

        os.chdir(dirName)
        mypath = os.getcwd()
        files = [ f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f)) ]

        date = dt.date(int(dirName[5:9]), int(dirName[10:12]), int(dirName[13:]))
        print date

        CCILs = {'CCIL01.xls' : lambda x: self.read_CCIL01(x), 'CCIL02.xls' : lambda x: self.read_CCIL02(x),
                 'CCIL0809.xls' : lambda x: self.read_CCIL0809(x), 'CCIL10.xls' : lambda x: self.read_CCIL10(x),
                 'CCIL1213.xls' : lambda x: self.read_CCIL1213(x), 'CCIL14.xls' : lambda x: self.read_CCIL14(x),
                 'CCIL15.xls' : lambda x: self.read_CCIL15(x), 'CCIL16.xls' : lambda x: self.read_CCIL16(x),
                 'CCIL1718.xls' : lambda x: self.read_CCIL1718(x), 'CCIL19.xls' : lambda x: self.read_CCIL19(x),
                 'CCIL20.xls' : lambda x: self.read_CCIL20(x), 'CCIL21.xls' : lambda x: self.read_CCIL21(x),
                 'CCIL22.xls' : lambda x: self.read_CCIL22(x), 'CCIL23.xls' : lambda x: self.read_CCIL23(x),
                 'CCIL24.xls' : lambda x: self.read_CCIL24(x), 'CCIL26.xls' : lambda x: self.read_CCIL26(x),
                 'CCIL27.xls' : lambda x: self.read_CCIL27(x), 'CCIL29.xls' : lambda x: self.read_CCIL29(x),
                 'CCIL30.xls' : lambda x: self.read_CCIL30(x), 'CCIL31.xls' : lambda x: self.read_CCIL31(x),
                 'CCIL32.xls' : lambda x: self.read_CCIL32(x), 'CCIL33.xls' : lambda x: self.read_CCIL33(x),
                 'CCIL03.xls' : lambda x: x, 'CCIL0407.xls' : lambda x: x, 'CCIL25.xls' : lambda x: x}

        p = re.compile(r'CCIL.+')
        for f in files:
            if( p.search(f) ):
                CCILs[ p.search(f).group() ](date)

        #-------------------------------------------------------CBLO-------------------------------------------------------------------
        p = re.compile(r'cblo_.+')
        for f in files:
            m = p.match(f)
            if m != None:
                cblo_file_name = m.group()
                break

        self.get_cblo_csv(cblo_file_name, date)

        #------------------------------------------------------gov_sec-----------------------------------------------------------------

        p = re.compile(r'gsec_.+')
        for f in files:
            m = p.match(f)
            if m != None:
                gsec_master_file = m.group()

        self.get_gsec_master(gsec_master_file, date)

        #------------------------------------------------------trade data---------------------------------------------------------------

        p = re.compile(r'outright_.+')
        for f in files:
            m = p.match(f)
            if m != None:
                outright_fileName = m.group()
                break

        p = re.compile(r'repo_.+')
        for f in files:
            m = p.match(f)
            if m != None:
                reported_fileName = m.group()
                break

        self.get_trade_csv(outright_fileName, reported_fileName, date)


        os.chdir(self._dirName)


    def write_to_db(self):

        query = self.get_queries()

        self.cur.executemany(query['cblo'], self._cblo)

        self.cur.executemany(query['trade_data'], self._trade_data)

        self.cur.executemany(query['forex_vol'], self._forex_vol)

        self.cur.executemany(query['benchmark_rates'], self._benchmark_rates)

        self.cur.executemany(query['trade_summary'], self._trade_summary)

        self.cur.executemany(query['term_money'], self._term_money_outstanding)

        self.cur.executemany(query['forex_intercategory'], self._forex_intercategory)

        self.cur.executemany(query['repo_intercategory'], self._repo_intercategory)

        self.cur.executemany(query['gsec_otc_ndsom'], self._gsec_market_share['NDS-OM/OTC'])

        self.cur.executemany(query['gsec_constituent_deals'], self._gsec_market_share['Constituent Deals'])

        self.cur.executemany(query['otc_outright_forex_intercategory'], self._otc_outright_forex_intercategory)

        self.cur.executemany(query['otc_gsec_intercategory'], self._otc_gsec_intercategory)

        self.cur.executemany(query['otc_tbill_intercategory'], self._otc_tbill_intercategory)

        self.cur.executemany(query['otc_trade_details'], self._otc_trade_details)

        self.cur.executemany(query['ndsom_trade_details'], self._ndsom_trade_details)

        self.cur.executemany(query['when_issued_trade_details'], self._when_issued_trade_details)

        self.cur.executemany(query['irs_transactions'], self._irs_transactions)

        self.cur.executemany(query['sdl_index'], self._sdl_index)

        self.cur.executemany(query['bond_index'], self._bond_index)

        self.cur.executemany(query['tenor_index'], self._tenor_index)

        self.cur.executemany(query['tbill_index'], self._tbill_index)

        self.cur.executemany(query['all_sovr_bond_index'], self._all_sovr_bond_index)

        self.cur.executemany(query['spot_rate'], self._spot_rate)

        self.cur.executemany(query['state_gsec_spread'], self._state_gsec_spread)

        self.cur.executemany(query['ccbid_ccbor'], self._ccbid_ccbor)

        self.cur.executemany(query['mibid_mibor'], self._mibid_mibor)

        self.con.commit()

        for s in self._gov_sec:
            try:
                self.cur.execute(query['gsec_master'], s)

            except psycopg2.IntegrityError:
                self.con.rollback()
                self.cur.execute('UPDATE gsec_master SET outstanding = (%s) where isin = (%s)', (s[3], s[0]))

            else:
                self.con.commit()

        self.con.commit()

        self.cur.close()
        self.con.close()


    def get_queries(self):

        column_str = dict()
        insert_str = dict()
        query = dict()

        column_str['cblo'] = 'transaction_date, instrument, market_type, days_to_maturity, settlement_type, maturity_date, \
                                number_of_trades, volume, open_price, high_price, low_price, close_price, weighted_avg'
        insert_str['cblo'] = ('%s, '*13)[:-2]
        query['cblo'] = 'INSERT INTO cblo (%s) VALUES (%s)' % (column_str['cblo'], insert_str['cblo'])

        column_str['trade_data'] = 'security_id, sec_description, maturity_date, transaction_date, trade_type, number_of_trades, total_volume, \
                                    open_price, high_price, low_price, last_traded_price, weighted_avg_price, weighted_avg_yield'
        insert_str['trade_data'] = ('%s, '*13)[:-2]
        query['trade_data'] = 'INSERT INTO trade_data (%s) VALUES (%s)' % (column_str['trade_data'], insert_str['trade_data'])

        column_str['forex_vol'] = 'summary_date, cash_no_of_trades, cash_usd, cash_inr, tom_no_of_trades, tom_usd, tom_inr, \
                                spot_no_of_trades, spot_usd, spot_inr, forward_no_of_trades, forward_usd, forward_inr'
        insert_str['forex_vol'] = ('%s, '*13)[:-2]
        query['forex_vol'] = 'INSERT INTO forex_volumes (%s) VALUES (%s)' % (column_str['forex_vol'], insert_str['forex_vol'])

        column_str['benchmark_rates'] = 'dates, '
        lst = np.arange(0.5, 30.5, 0.5)
        for el in lst:
            column_str['benchmark_rates'] += '"' + str(el) + '", '

        insert_str['benchmark_rates'] = ('%s, '*61)[:-2]
        query['benchmark_rates'] = 'INSERT INTO benchmark_rates (%s) VALUES (%s)' % \
            (column_str['benchmark_rates'][:-2], insert_str['benchmark_rates'])

        column_str['trade_summary'] = 'summary_date, description, maturity, trades, volume, high, low, last_traded_price, last_traded_ytm, \
                                        weighted_avg_price, weighted_avg_yield'
        insert_str['trade_summary'] = ('%s, '*11)[:-2]
        query['trade_summary'] = 'INSERT INTO trade_summary (%s) VALUES (%s)' % (column_str['trade_summary'], insert_str['trade_summary'])

        column_str['term_money'] = 'summary_date, settlement_date, maturity_date, original_tenor, trades, amount_cr, residual_days, rate'
        insert_str['term_money'] = ('%s, '*8)[:-2]
        query['term_money'] = 'INSERT INTO term_money_stats (%s) VALUES (%s)' % (column_str['term_money'], insert_str['term_money'])

        column_str['forex_intercategory'] = 'summary_date, market_type, category, foreign_banks, public_sector_banks, private_sector_banks, \
                                            cooperative_banks, financial_institutions, total, percentage_of_market_share'
        insert_str['forex_intercategory'] = ('%s, '*10)[:-2]
        query['forex_intercategory'] = 'INSERT INTO forex_intercategory (%s) VALUES (%s)' % (column_str['forex_intercategory'], insert_str['forex_intercategory'])

        column_str['repo_intercategory'] = 'summary_date, market_type, dealer, category, foreign_banks, public_sector_banks, private_sector_banks, \
                                            mutual_funds, others, primary_dealers, total, percentage_of_market_share'
        insert_str['repo_intercategory'] = ('%s, '*12)[:-2]
        query['repo_intercategory'] = 'INSERT INTO repo_intercategory (%s) VALUES (%s)' % (column_str['repo_intercategory'], insert_str['repo_intercategory'])

        column_str['gsec_otc_ndsom'] = 'summary_date, category, outright_buy, outright_sell, gsec_buy, gsec_sell, tbill_buy, tbill_sell, sdl_buy, sdl_sell'
        insert_str['gsec_otc_ndsom'] = ('%s, '*10)[:-2]
        query['gsec_otc_ndsom'] = 'INSERT INTO gsec_otc_ndsom (%s) VALUES (%s)' % (column_str['gsec_otc_ndsom'], insert_str['gsec_otc_ndsom'])

        column_str['gsec_constituent_deals'] = 'summary_date, category, trades, volume, percentage_total_outright_volume'
        insert_str['gsec_constituent_deals'] =  ('%s, '*5)[:-2]
        query['gsec_constituent_deals'] = 'INSERT INTO gsec_constituent_deals (%s) VALUES (%s)' % (column_str['gsec_constituent_deals'], insert_str['gsec_constituent_deals'])

        column_str['otc_outright_forex_intercategory'] = 'summary_date, dealer, category, foreign_banks, public_sector_banks, \
                                        private_sector_banks, mutual_funds, others, primary_dealers, total, percentage_of_market_share'
        insert_str['otc_outright_forex_intercategory'] = ('%s, '*11)[:-2]
        query['otc_outright_forex_intercategory'] = 'INSERT INTO otc_outright_forex_intercategory (%s) VALUES (%s)' % \
                                                    (column_str['otc_outright_forex_intercategory'], insert_str['otc_outright_forex_intercategory'])

        column_str['otc_gsec_intercategory'] = 'summary_date, dealer, category, foreign_banks, public_sector_banks, \
                                        private_sector_banks, mutual_funds, others, primary_dealers, total, percentage_of_market_share'
        insert_str['otc_gsec_intercategory'] = ('%s, '*11)[:-2]
        query['otc_gsec_intercategory'] = 'INSERT INTO otc_gsec_intercategory (%s) VALUES (%s)' % (column_str['otc_gsec_intercategory'], insert_str['otc_gsec_intercategory'])

        column_str['otc_tbill_intercategory'] = 'summary_date, dealer, category, foreign_banks, public_sector_banks, \
                                        private_sector_banks, mutual_funds, others, primary_dealers, total, percentage_of_market_share'
        insert_str['otc_tbill_intercategory'] = ('%s, '*11)[:-2]
        query['otc_tbill_intercategory'] = 'INSERT INTO otc_tbill_intercategory (%s) VALUES (%s)' % (column_str['otc_tbill_intercategory'], insert_str['otc_tbill_intercategory'])

        column_str['otc_trade_details'] =  'summary_date, description, trades, volume, open, high, low, close, weighted_avg_price, weighted_avg_yield'
        insert_str['otc_trade_details'] = ('%s, '*10)[:-2]
        query['otc_trade_details'] = 'INSERT INTO otc_trade_details (%s) VALUES (%s)' % (column_str['otc_trade_details'], insert_str['otc_trade_details'])

        column_str['ndsom_trade_details'] = 'summary_date, description, trades, volume, open, high, low, close, weighted_avg_price, weighted_avg_yield'
        insert_str['ndsom_trade_details'] = ('%s, '*10)[:-2]
        query['ndsom_trade_details'] = 'INSERT INTO ndsom_trade_details (%s) VALUES (%s)' % (column_str['ndsom_trade_details'], insert_str['ndsom_trade_details'])

        column_str['when_issued_trade_details'] = 'summary_date, description, maturity_date, trades, volume, high, low, weighted_avg_price, weighted_avg_yield'
        insert_str['when_issued_trade_details'] = ('%s, '*9)[:-2]
        query['when_issued_trade_details'] = 'INSERT INTO when_issued_trade_details (%s) VALUES (%s)' % (column_str['when_issued_trade_details'], insert_str['when_issued_trade_details'])

        column_str['irs_transactions'] = 'summary_date, benchmark, tenors, trades, volume, high, low, close, last_reported_rate, weighted_avg_rate'
        insert_str['irs_transactions'] = ('%s, '*10)[:-2]
        query['irs_transactions'] = 'INSERT INTO irs_transactions (%s) VALUES (%s)' % (column_str['irs_transactions'], insert_str['irs_transactions'])

        column_str['sdl_index'] = 'summary_date, ccil_sdl_pri, ccil_sdl_tri, coupon, yield, duration'
        insert_str['sdl_index'] = ('%s, '*6)[:-2]
        query['sdl_index'] = 'INSERT INTO sdl_index (%s) VALUES (%s)' % (column_str['sdl_index'], insert_str['sdl_index'])

        column_str['bond_index'] = 'summary_date, ccil_broad_tri, ccil_broad_pri, ccil_liquid_01, ccil_liquid_02'
        insert_str['bond_index'] = ('%s, '*5)[:-2]
        query['bond_index'] = 'INSERT INTO bond_index (%s) VALUES (%s)' % (column_str['bond_index'], insert_str['bond_index'])

        column_str['tenor_index'] = 'summary_date, type, upto_5, years_5_10, years_10_15, years_15_20, years_20_30'
        insert_str['tenor_index'] = ('%s, '*7)[:-2]
        query['tenor_index'] = 'INSERT INTO tenor_index (%s) VALUES (%s)' % (column_str['tenor_index'], insert_str['tenor_index'])

        column_str['tbill_index'] = 'summary_date, ccil_liquidity_weight_index, duration_lwi, ccil_equal_weight_index, duration_ewi'
        insert_str['tbill_index'] = ('%s, '*5)[:-2]
        query['tbill_index'] = 'INSERT INTO tbill_index (%s) VALUES (%s)' % (column_str['tbill_index'], insert_str['tbill_index'])

        column_str['all_sovr_bond_index'] = 'summary_date, casbi_tri, casbi_pri, coupon, duration'
        insert_str['all_sovr_bond_index'] = ('%s, '*5)[:-2]
        query['all_sovr_bond_index'] = 'INSERT INTO all_sovreign_bond_index (%s) VALUES (%s)' % (column_str['all_sovr_bond_index'], insert_str['all_sovr_bond_index'])

        column_str['spot_rate'] = 'summary_date, rate, high, low, volatility'
        insert_str['spot_rate'] = ('%s, '*5)[:-2]
        query['spot_rate'] = 'INSERT INTO spot_rate (%s) VALUES (%s)' % (column_str['spot_rate'], insert_str['spot_rate'])

        column_str['state_gsec_spread'] = 'summary_date, description, maturity_date, trades, volume, avg_spread, close_price, close_ytm, weighted_avg_price, weighted_avg_yield'
        insert_str['state_gsec_spread'] = ('%s, '*10)[:-2]
        query['state_gsec_spread'] = 'INSERT INTO state_gsec_spread (%s) VALUES (%s)' % (column_str['state_gsec_spread'], insert_str['state_gsec_spread'])

        column_str['ccbid_ccbor'] = 'summary_date, time, ccbor, ccbor_st_dev, ccbid, ccbid_st_dev'
        insert_str['ccbid_ccbor'] = ('%s, '*6)[:-2]
        query['ccbid_ccbor'] = 'INSERT INTO ccbid_ccbor (%s) VALUES (%s)' % (column_str['ccbid_ccbor'], insert_str['ccbid_ccbor'])

        column_str['mibid_mibor'] = 'summary_date, time, mibor, mibor_volatility, mibid, mibid_volatility, waonr, dealta'
        insert_str['mibid_mibor'] = ('%s, '*8)[:-2]
        query['mibid_mibor'] = 'INSERT INTO mibid_mibor (%s) VALUES (%s)' % (column_str['mibid_mibor'], insert_str['mibid_mibor'])

        column_str['gsec_master'] = 'isin, interest_rate, maturity_date, outstanding, security_type'
        insert_str['gsec_master'] = ('%s, '*5)[:-2]
        query['gsec_master'] = 'INSERT INTO gsec_master (%s) VALUES (%s)' % (column_str['gsec_master'], insert_str['gsec_master'])

        return query


    def get_cblo_csv(self, fileName, date):

        cblo = csv.reader(open(fileName), delimiter = ',')
        cblo.next()
        for row in cblo:
            if row[0] == '': break
            c1, c2, c3, c4 = row[0], row[1], int(row[2]), int(row[3][2])
            c5 = dt.timedelta( days = int(row[2]) ) + date
            c6 = int(row[4])
            try:
                c7 =  float(row[5].replace(',', ''))
            except:
                c7 = 0.0
            try:
                c8 =  float(row[6].replace(',', ''))
            except:
                c8 = 0.0
            try:
                c9 =  float(row[7].replace(',', ''))
            except:
                c9 = 0.0
            try:
                c10 =  float(row[8].replace(',', ''))
            except:
                c10 = 0.0
            try:
                c11 =  float(row[9].replace(',', ''))
            except:
                c11 = 0.0
            try:
                c12 =  float(row[10].replace(',', ''))
            except:
                c12 = 0.0

            self._cblo.add( tuple( [ date, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12 ] ) )


    def get_trade_csv(self, fileName1, fileName2, date):
        months_map = { 'JAN': 1, 'FEB':2, 'MAR':3, 'APR':4, 'MAY':5, 'JUN':6, 'JUL':7, 'AUG':8, 'SEP':9, 'OCT':10, 'NOV':11, 'DEC':12 }
        get_d = lambda x: tuple( [ int(x[0]), months_map[x[1]], int(x[2]) ] )

        outright = csv.reader( open(fileName1), delimiter = ',')
        outright.next()
        for row in outright:
            self._trade_data.add( tuple( [ row[0], row[1], dt.date(*(get_d(row[2].split('-')))), date,\
                                          'Outright', int(row[3]), float(row[4]), float(row[5]), float(row[6]), \
                                          float(row[7]), float(row[8]), float(row[9]), float(row[10]) ] ) )

        repo = csv.reader( open(fileName2), delimiter = ',' )
        repo.next()
        for row in repo:
            self._trade_data.add( tuple( [ row[0], row[1], dt.date(*(get_d(row[2].split('-')))), date,\
                                          'Reported', int(row[3]), float(row[4]), float(row[5]), float(row[6]), \
                                          float(row[7]), float(row[8]), float(row[9]), float(row[10]) ] ) )


    def get_gsec_master(self, fileName, date):

        months_map = { 'Jan': 1, 'Feb':2, 'Mar':3, 'Apr':4, 'May':5, 'Jun':6, 'Jul':7, 'Aug':8, 'Sep':9, 'Oct':10, 'Nov':11, 'Dec':12 }
        get_d = lambda x:  tuple( [ 2000+int(x[2]), months_map[x[1]], int(x[0]) ] ) if len(x)==3 else xlrd.xldate_as_tuple(int(x[0]), 0)[:3]

        gsec = csv.reader(open(fileName), delimiter = ',')
        test = len(gsec.next())
        if test == 5:
            for row in gsec:
                c1 = row[0]

                try:
                    c2 = float(re.compile(r'.+%').match(row[1]).group()[:-1])
                except:
                    c2 = float(row[1])*100

                c3 = dt.date(*(get_d(row[2].split('-'))))

                try:
                    c4 = int(row[3])
                except:
                    c4 = int(row[3][:row[3].index('.')])

                c5 = row[4] if row[4] != '' else 'UNKNOWN'

                self._gov_sec.add( tuple( [ c1, c2, c3, c4, c5 ] ) )
        else:
            for row in gsec:
                c1 = row[0]

                try:
                    c2 = float(re.compile(r'.+%').match(row[1]).group()[:-1])
                except:
                    c2 = float(row[1])*100

                c3 = dt.date(*(get_d(row[3].split('-'))))

                try:
                    c4 = int(row[4])
                except:
                    c4 = int(row[4][:row[4].index('.')]) if row[4].rfind('E') == -1 else int(float(row[4]))

                c5 = row[5] if row[5] != '' else 'UNKNOWN'

                self._gov_sec.add( tuple( [ c1, c2, c3, c4, c5 ] ) )


    def read_CCIL01(self, date):

        book = xlrd.open_workbook('CCIL01.xls')
        sheet = book.sheet_by_index(0)

        self._benchmark_rates.add( tuple( sum( [ [date], sheet.col_values(1, 1, sheet.nrows) ], []  ) ) )


    def read_CCIL02(self, date):

        book = xlrd.open_workbook('CCIL02.xls')
        sheet = book.sheet_by_index(2)

        self._forex_vol.add( tuple( sum( [ [date], sheet.row_values(8, 1, 4),
                                              sheet.row_values(9, 1, 4),
                                              sheet.row_values(10, 1, 4),
                                              sheet.row_values(11, 1, 4) ], [] ) ) )

        sheet = book.sheet_by_index(1)

        for i in range(1, sheet.nrows-1):
            row = sheet.row_values(i)
            row = filter( lambda x: x!='', [ dt.datetime(*(xlrd.xldate_as_tuple(x, book.datemode))) if row.index(x) == 1 else x for x in row ] )
            if( len(row) > 0 ) : self._trade_summary.add( tuple( sum( [ [date], row ], [] ) ) )


    def read_CCIL0809(self, date):

        book = xlrd.open_workbook('CCIL0809.xls')
        sheet = book.sheet_by_index(0)

        tables = { 'Buy Side' : range(4,10), 'Sell Side' : range(14,20) }

        for key in tables.iterkeys():
            for row in tables[key]:
                self._otc_outright_forex_intercategory.add( tuple( sum( [ [date, key], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL10(self, date):

        book = xlrd.open_workbook('CCIL10.xls')
        sheet = book.sheet_by_index(1)

        for row in range( 1, sheet.nrows ):
            values = sheet.row_values(row)
            self._term_money_outstanding.add( tuple( sum( [ [date], \
            filter(lambda x: x != '', [ dt.datetime(*(xlrd.xldate_as_tuple(x, book.datemode))) if values.index(x) == 0 or values.index(x) == 1 else x for x in values ]) ], [] ) ) )


    def read_CCIL1213(self, date):

        book = xlrd.open_workbook('CCIL1213.xls')
        sheet = book.sheet_by_index(0)

        markets = { 'Cash' : range(6,11),
                    'Tom' : range(17,22),
                    'Spot' : range(28,33),
                    'Forward' : range(39,44) }

        for mkt in markets.iterkeys():
            for row in markets[mkt]:
                self._forex_intercategory.add( tuple( sum( [ [date, mkt], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL14(self, date):

        book = xlrd.open_workbook('CCIL14.xls')
        sheet = book.sheet_by_index(0)

        tables = {'Reverse Repo'  : [ 'Lender', range(5,11) ],
                  'Repo'          : [ 'Borrower', range(15,21) ]}

        for key in tables.iterkeys():
            for row in tables[key][1]:
                self._repo_intercategory.add( tuple( sum( [ [ date, key, tables[key][0] ], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL15(self, date):

        book = xlrd.open_workbook('CCIL15.xls')
        sheet = book.sheet_by_index(0)

        for row in range(5,11):
            self._gsec_market_share['NDS-OM/OTC'].add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )

        for row in range(17,19):
            self._gsec_market_share['Constituent Deals'].add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL16(self, date):

        book = xlrd.open_workbook('CCIL16.xls')
        sheet = book.sheet_by_index(0)

        self._sdl_index.add( tuple( sum( [ [ date ], [ x if type(x)!=str else None for x in sheet.row_values(2,1,6)] ], [] ) ) )


    def read_CCIL1718(self, date):

        book = xlrd.open_workbook('CCIL1718.xls')
        sheet = book.sheet_by_index(0)

        for row in range(4, sheet.nrows):
            values = sheet.row_values(row)
            self._state_gsec_spread.add( tuple( sum( [ [date], \
            filter(None, [ dt.datetime(*(xlrd.xldate_as_tuple(x, book.datemode))) if values.index(x) == 1 else x for x in values ]) ], [] ) ) )


    def read_CCIL19(self, date):

        book = xlrd.open_workbook('CCIL19.xls')
        sheet = book.sheet_by_index(0)

        self._bond_index.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(2,1) if type(x)!=str ] ], [] ) ) )


    def read_CCIL20(self, date):

        book = xlrd.open_workbook('CCIL20.xls')
        sheet = book.sheet_by_index(0)

        for row in [2,3]:
            self._tenor_index.add( tuple( sum( [ [date], [ x if type(x)!=str else None for x in sheet.row_values(row,0,6) ] ], [] ) ) )


    def read_CCIL21(self, date):

        book = xlrd.open_workbook('CCIL21.xls')
        sheet = book.sheet_by_index(0)

        self._tbill_index.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(2,1) if type(x)!=str ] ], [] ) ) )


    def read_CCIL22(self, date):

        book = xlrd.open_workbook('CCIL22.xls')
        sheet = book.sheet_by_index(0)

        self._all_sovr_bond_index.add(tuple( sum( [ [ date ], [ x for x in sheet.row_values(2,1) if type(x)!=str ] ], [] ) )  )


    def read_CCIL23(self, date):

        book = xlrd.open_workbook('CCIL23.xls')
        sheet = book.sheet_by_index(0)

        tables = { 'Buy Side' : range(4,10), 'Sell Side' : range(14,20) }

        for key in tables.iterkeys():
            for row in tables[key]:
                self._otc_gsec_intercategory.add( tuple( sum( [ [date, key], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL24(self, date):

        book = xlrd.open_workbook('CCIL24.xls')
        sheet = book.sheet_by_index(0)

        tables = { 'Buy Side' : range(4,10), 'Sell Side' : range(14,20) }

        for key in tables.iterkeys():
            for row in tables[key]:
                self._otc_tbill_intercategory.add( tuple( sum( [ [date, key], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL26(self, date):

        book = xlrd.open_workbook('CCIL26.xls')
        sheet = book.sheet_by_index(0)

        for row in range(2, sheet.nrows):
            self._otc_trade_details.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL27(self, date):

        book = xlrd.open_workbook('CCIL27.xls')
        sheet = book.sheet_by_index(0)

        for row in range(2, sheet.nrows):
            self._ndsom_trade_details.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL29(self, date):

        book = xlrd.open_workbook('CCIL29.xls')
        sheet = book.sheet_by_index(0)

        for row in range(2, sheet.nrows-1):
            self._when_issued_trade_details.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(row) if type(x)!=str ] ], [] ) ) )


    def read_CCIL30(self, date):

        book = xlrd.open_workbook('CCIL30.xls')
        sheet = book.sheet_by_index(0)

        for row in [1,2]:
            self._ccbid_ccbor.add( tuple( sum( [ [ date ], [ x if type(x)!=str else None for x in sheet.row_values(row,1) ] ], [] ) ) )


    def read_CCIL31(self, date):

        book = xlrd.open_workbook('CCIL31.xls')
        sheet = book.sheet_by_index(0)

        for row in [1,2]:
            self._mibid_mibor.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(row,1) if type(x)!=str ] ], [] ) ) )


    def read_CCIL32(self, date):

        book = xlrd.open_workbook('CCIL32.xls')
        sheet = book.sheet_by_index(0)

        tables = { 'MIBOR' : range(3,15), 'MIFOR' : range(19,26) }

        for benchmark in tables.iterkeys():
            for i in tables[benchmark]:
                row = [ None if x == ' ' else x if type(x)!=str else None for x in sheet.row_values(i,0,8) ]
                self._irs_transactions.add( tuple( sum( [ [ date, benchmark ], row ], [] ) ) )


    def read_CCIL33(self, date):

        book = xlrd.open_workbook('CCIL33.xls')
        sheet = book.sheet_by_index(0)

        self._spot_rate.add( tuple( sum( [ [ date ], [ x for x in sheet.row_values(2,1) if type(x)!=str ] ], [] ) ) )


def init_db(cur, con):

    cur.execute('''CREATE TABLE IF NOT EXISTS cblo
	(
		instrument character varying(64),
		market_type character varying(32),
		transaction_date date,
		settlement_type integer,
		days_to_maturity integer,
		maturity_date date,
		number_of_trades integer,
		volume real,
		open_price real,
		high_price real,
		low_price real,
		close_price real,
		weighted_avg real
	)''')

    cur.execute('''CREATE TABLE IF NOT EXISTS forex_volumes
	(
		summary_date date,
		cash_no_of_trades integer,
		cash_usd real,
		cash_inr real,
		tom_no_of_trades integer,
		tom_usd real,
		tom_inr real,
		spot_no_of_trades integer,
		spot_usd real,
		spot_inr real,
		forward_no_of_trades integer,
		forward_usd real,
		forward_inr real
	)''')

    cur.execute('''CREATE TABLE IF NOT EXISTS trade_data
	(
		security_id character(12),
		sec_description character varying(32),
		transaction_date date,
		maturity_date date,
		trade_type character varying(8),
		number_of_trades integer,
		total_volume real,
		open_price real,
		high_price real,
		low_price real,
		last_traded_price real,
		last_traded_ytm real,
		weighted_avg_price real,
		weighted_avg_yield real
	)''')

    cur.execute('''CREATE TABLE IF NOT EXISTS benchmark_rates
    (
        Dates date,
		"0.5" real,
		"1.0" real,
		"1.5" real,
		"2.0" real,
		"2.5" real,
		"3.0" real,
		"3.5" real,
		"4.0" real,
		"4.5" real,
		"5.0" real,
		"5.5" real,
		"6.0" real,
		"6.5" real,
		"7.0" real,
		"7.5" real,
		"8.0" real,
		"8.5" real,
		"9.0" real,
		"9.5" real,
		"10.0" real,
		"10.5" real,
		"11.0" real,
		"11.5" real,
		"12.0" real,
		"12.5" real,
		"13.0" real,
		"13.5" real,
		"14.0" real,
		"14.5" real,
		"15.0" real,
		"15.5" real,
		"16.0" real,
		"16.5" real,
		"17.0" real,
		"17.5" real,
		"18.0" real,
		"18.5" real,
		"19.0" real,
		"19.5" real,
		"20.0" real,
		"20.5" real,
		"21.0" real,
		"21.5" real,
		"22.0" real,
		"22.5" real,
		"23.0" real,
		"23.5" real,
		"24.0" real,
		"24.5" real,
		"25.0" real,
		"25.5" real,
		"26.0" real,
		"26.5" real,
		"27.0" real,
		"27.5" real,
		"28.0" real,
		"28.5" real,
		"29.0" real,
		"29.5" real,
		"30.0" real,
        CONSTRAINT "PKey" PRIMARY KEY (dates)
	)''')

    cur.execute('''CREATE TABLE IF NOT EXISTS trade_summary
    (
          summary_date date,
          description character varying,
          maturity date,
          trades real,
          volume real,
          high real,
          low real,
          last_traded_price real,
          last_traded_ytm real,
          weighted_avg_price real,
          weighted_avg_yield real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS term_money_stats
    (
          summary_date date,
          settlement_date date,
          maturity_date date,
          original_tenor character varying,
          trades real,
          amount_cr real,
          residual_days integer,
          rate real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS forex_intercategory
    (
          summary_date date,
          market_type character varying,
          category character varying,
          foreign_banks real,
          public_sector_banks real,
          private_sector_banks real,
          cooperative_banks real,
          financial_institutions real,
          total real,
          percentage_of_market_share real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS repo_intercategory
    (
          summary_date date,
          market_type character varying,
          dealer character varying,
          category character varying,
          foreign_banks real,
          public_sector_banks real,
          private_sector_banks real,
          mutual_funds real,
          others real,
          primary_dealers real,
          total real,
          percentage_of_market_share real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS gsec_otc_ndsom
    (
          summary_date date,
          category character varying,
          outright_buy real,
          outright_sell real,
          gsec_buy real,
          gsec_sell real,
          tbill_buy real,
          tbill_sell real,
          sdl_buy real,
          sdl_sell real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS gsec_constituent_deals
    (
          summary_date date,
          category character varying,
          trades integer,
          volume real,
          percentage_total_outright_volume real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS otc_outright_forex_intercategory
    (
          summary_date date,
          dealer character varying,
          category character varying,
          foreign_banks real,
          public_sector_banks real,
          private_sector_banks real,
          mutual_funds real,
          others real,
          primary_dealers real,
          total real,
          percentage_of_market_share real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS otc_gsec_intercategory
    (
          summary_date date,
          dealer character varying,
          category character varying,
          foreign_banks real,
          public_sector_banks real,
          private_sector_banks real,
          mutual_funds real,
          others real,
          primary_dealers real,
          total real,
          percentage_of_market_share real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS otc_tbill_intercategory
    (
          summary_date date,
          dealer character varying,
          category character varying,
          foreign_banks real,
          public_sector_banks real,
          private_sector_banks real,
          mutual_funds real,
          others real,
          primary_dealers real,
          total real,
          percentage_of_market_share real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS otc_trade_details
    (
          summary_date date,
          description character varying,
          trades integer,
          volume real,
          open real,
          high real,
          low real,
          close real,
          weighted_avg_price real,
          weighted_avg_yield real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS ndsom_trade_details
    (
          summary_date date,
          description character varying,
          trades integer,
          volume real,
          open real,
          high real,
          low real,
          close real,
          weighted_avg_price real,
          weighted_avg_yield real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS when_issued_trade_details
    (
        summary_date date,
        description character varying,
        maturity_date date,
        trades integer,
        volume real,
        high real,
        low real,
        weighted_avg_price real,
        weighted_avg_yield real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS irs_transactions
    (
          summary_date date,
          benchmark character varying,
          tenors character varying,
          trades integer,
          volume real,
          high real,
          low real,
          close real,
          last_reported_rate real,
          weighted_avg_rate real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS sdl_index
    (
        summary_date date,
        ccil_sdl_pri real,
        ccil_sdl_tri real,
        coupon real,
        yield real,
        duration real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS bond_index
    (
        summary_date date,
        ccil_broad_tri real,
        ccil_broad_pri real,
        ccil_liquid_01 real,
        ccil_liquid_02 real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS tenor_index
    (
        summary_date date,
        type character varying,
        upto_5 real,
        years_5_10 real,
        years_10_15 real,
        years_15_20 real,
        years_20_30 real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS tbill_index
    (
        summary_date date,
        ccil_liquidity_weight_index real,
        duration_lwi real,
        ccil_equal_weight_index real,
        duration_ewi real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS all_sovreign_bond_index
    (
        summary_date date,
        casbi_tri real,
        casbi_pri real,
        coupon real,
        duration real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS spot_rate
    (
        summary_date date,
        rate real,
        high real,
        low real,
        volatility real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS state_gsec_spread
    (
        summary_date date,
        description character varying,
        maturity_date date,
        trades integer,
        volume real,
        avg_spread real,
        close_price real,
        close_ytm real,
        weighted_avg_price real,
        weighted_avg_yield real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS ccbid_ccbor
    (
        summary_date date,
        time character varying,
        ccbor real,
        ccbor_st_dev real,
        ccbid real,
        ccbid_st_dev real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS mibid_mibor
    (
        summary_date date,
        time character varying,
        mibor real,
        mibor_volatility real,
        mibid real,
        mibid_volatility real,
        waonr real,
        dealta real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS cds_transaction_summary
    (
        summary_date date,
        tenor character varying,
        trades real,
        notional_amt real,
        reference_entities real,
        high_bps real,
        low_bps real
    )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS gsec_master
    (
        isin character varying NOT NULL,
        interest_rate real NOT NULL,
        maturity_date date NOT NULL,
        outstanding bigint NOT NULL,
        security_type character varying NOT NULL,
        CONSTRAINT pkey PRIMARY KEY (isin)
    )''')

    con.commit()


def unzip(zipFilePath, destDir):

    zfile = zipfile.ZipFile(zipFilePath)
    if not os.path.exists(destDir):
        os.mkdir(destDir)

    for name in zfile.namelist():
        (dirName, fileName) = os.path.split(name)
        if fileName == '':
            newDir = destDir + '/' +dirName
            if not os.path.exists(newDir):
                os.mkdir(newDir)

        else:
            zfile.extract(member = name, path = destDir)


def init_files(dirName):

    os.chdir(dirName)

    files = [ f for f in os.listdir(dirName) if os.path.isfile(os.path.join(dirName, f)) ]

    months = { re.compile(r'DFIV_\d{,2}_01_\d{,4}.zip') : '01-', re.compile(r'DFIV_\d{,2}_02_\d{,4}.zip') : '02-', \
            re.compile(r'DFIV_\d{,2}_03_\d{,4}.zip') : '03-', re.compile(r'DFIV_\d{,2}_04_\d{,4}.zip') : '04-', \
            re.compile(r'DFIV_\d{,2}_05_\d{,4}.zip') : '05-', re.compile(r'DFIV_\d{,2}_06_\d{,4}.zip') : '06-', \
            re.compile(r'DFIV_\d{,2}_07_\d{,4}.zip') : '07-', re.compile(r'DFIV_\d{,2}_08_\d{,4}.zip') : '08-', \
            re.compile(r'DFIV_\d{,2}_09_\d{,4}.zip') : '09-', re.compile(r'DFIV_\d{,2}_10_\d{,4}.zip') : '10-', \
            re.compile(r'DFIV_\d{,2}_11_\d{,4}.zip') : '11-', re.compile(r'DFIV_\d{,2}_12_\d{,4}.zip') : '12-' }

    years = { re.compile(r'.+2011.zip') : '2011-', re.compile(r'.+2012.zip') : '2012-', re.compile(r'.+2013.zip') : '2013-', \
            re.compile(r'.+2014.zip') :'2014-' }

    for f in files:
        for m in months.iterkeys():
            month_match = m.match(f)
            if month_match != None:
                for y in years.iterkeys():
                    year_match = y.match(f)
                    if year_match != None:
                        newFileName = "DFIV_"+years[y]+months[m]+f[5:7]+".zip"
                        os.rename(f, newFileName)
                        break
                break

    os.mkdir('FORMAT1')
    os.mkdir('FORMAT2')

    files = [ f for f in os.listdir(dirName) if os.path.isfile(os.path.join(dirName, f)) ]
    files.sort()

    for f,i in zip(files,range(663)):
        shutil.move(f,'FORMAT1')

    files = [ f for f in os.listdir(dirName) if os.path.isfile(os.path.join(dirName, f)) ]

    for f in files:
        shutil.move(f,'FORMAT2')

    dirName1, dirName2 = dirName+'/FORMAT1', dirName+'/FORMAT2'
    return dirName1, dirName2


def get_historical_data(dirName1, dirName2, cur, con):

    f1 = format1(dirName1, cur, con)
    f1.read()
    f1.write_to_db()
    f2 = format2(dirName2, cur, con)
    f2.read()
    f2.write_to_db()


def main():

    con = psycopg2.connect(database = str(sys.argv[2]), user = str(sys.argv[3]))
    cur = con.cursor()

    init_db(cur, con)

    dirName1, dirName2 = init_files( str(sys.argv[1]) )

    get_historical_data(dirName1, dirName2, cur, con)


if __name__ == '__main__':
    main()
